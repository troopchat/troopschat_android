package org.chat.troopschat.contactshare;

public interface Selectable {
  void setSelected(boolean selected);
  boolean isSelected();
}
