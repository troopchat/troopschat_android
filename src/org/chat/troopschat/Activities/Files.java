package org.chat.troopschat.Activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;

import org.chat.troopschat.Adapter.FilesAdapter;
import org.chat.troopschat.ApplicationPreferencesActivity;
import org.chat.troopschat.Pojo.FilesPojo;
import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;
import org.chat.troopschat.Utils.CustomRecyclerView;
import org.chat.troopschat.Utils.Filepicker.MaterialFilePicker;
import org.chat.troopschat.Utils.Filepicker.ui.FilePickerActivity;
import org.chat.troopschat.Utils.Interfaces.Folderclick;
import org.chat.troopschat.Utils.Sessionmanager;
import org.chat.troopschat.Utils.rest.ApiClient;
import org.chat.troopschat.Utils.rest.ApiInterface;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.chat.troopschat.Utils.Constants.Capture_camera;
import static org.chat.troopschat.Utils.Constants.Pick_gallery;


public class Files extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, View.OnClickListener, Folderclick {
    public ApiInterface service;
    public ArrayList<FilesPojo> Folderlist = new ArrayList<>();
    CustomRecyclerView files_rv;
    BottomNavigationView file_bottom;
    TextView title_tv, oops_tv, create_tv, cancel_tv, search_et;
    ImageView add_iv, profile_iv, toggle_iv, search_iv, close;
    LinearLayout bottom_sheet, cam_ln, createFolder_ln, upload_ln, option_ln, create_rl;
    BottomSheetDialog sheetBehavior;
    View toolbar;
    Bitmap bm;
    String items;
    EditText name_tv;
    File choosedFile;

    boolean folderdata, all = true, recent, shared;
    boolean isList = true;
    ArrayList<FilesPojo> filesPojos = new ArrayList<>();
    String folder_id = "";
    Uri outuri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_files);
        service = ApiClient.createService(ApiInterface.class, Files.this,new Sessionmanager(this).getToken().userId);
        initViews();
    }

    private void initViews() {
        files_rv = findViewById(R.id.files_rv);
        oops_tv = findViewById(R.id.oops_tv);
        profile_iv = findViewById(R.id.profile_iv);
        toolbar = findViewById(R.id.toolbar);
        file_bottom = findViewById(R.id.file_bottom);
        title_tv = findViewById(R.id.title_tv);
        add_iv = findViewById(R.id.add_iv);
        bottom_sheet = findViewById(R.id.bottom_sheet);
        toggle_iv = findViewById(R.id.toggle_iv);
        search_iv = findViewById(R.id.search_iv);
        search_et = findViewById(R.id.search_et);
        sheetBehavior = new BottomSheetDialog(this);
        files_rv.setHasFixedSize(true);
        file_bottom.getMenu().getItem(3).setVisible(false);
        initClick();
        try {
            setData();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    private void initClick() {
        file_bottom.setOnNavigationItemSelectedListener(this);
        add_iv.setOnClickListener(this);
        search_iv.setOnClickListener(this);
        toggle_iv.setOnClickListener(this);
        profile_iv.setOnClickListener(this);
        search_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                filter(editable.toString());
            }
        });
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<FilesPojo> filterdNames = new ArrayList<>();

        //looping through existing elements
        for (int i = 0; i < filesPojos.size(); i++) {
            //if the existing elements contains the search input
            if (filesPojos.get(i).name.toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(filesPojos.get(i));
            }
        }

        if (isList)
            files_rv.setLayoutManager(new LinearLayoutManager(Files.this));
        else files_rv.setLayoutManager(new GridLayoutManager(Files.this, 3));
        files_rv.setAdapter(new FilesAdapter(Files.this, toolbar, filterdNames, isList, sheetBehavior, Files.this, folderdata, all, recent, shared));
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(Files.this, Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(Files.this, Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat.checkSelfPermission(Files.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(Files.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(Files.this, Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(Files.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Snackbar.make(Files.this.findViewById(android.R.id.content), "Please Grant Permissions to upload profile photo", Snackbar.LENGTH_INDEFINITE).setAction("ENABLE", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 012);
                    }
                }).show();
            } else {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 012);
            }
        } else {
            selectImage();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            Toast.makeText(getApplicationContext(), "Try Downloading Now", Toast.LENGTH_LONG).show();
        } else {
            boolean cam_Permission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
            boolean write_Permission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
            boolean storage_Permission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
            if (write_Permission && storage_Permission && cam_Permission) {
                selectImage();
            }
        }
    }

    private void selectImage() {
        if (items.equals("Take Photo")) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());

            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Date date = new Date();
            DateFormat df = new SimpleDateFormat("-mm-ss");

            String newPicFile = df.format(date) + ".jpg";
            String outPath = "/sdcard/" + newPicFile;
            File outFile = new File(outPath);

             outuri = Uri.fromFile(outFile);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outuri);

            startActivityForResult(takePictureIntent, Capture_camera);
        } else if (items.equals("Library")) {
            MaterialFilePicker filePicker = new MaterialFilePicker()
                    .withActivity(Files.this)
                    .withHiddenFiles(true)
                    .withRequestCode(Pick_gallery);
            filePicker.start();

//            Intent intent = new Intent();
//            intent.setType("*/*");
//            intent.setAction(Intent.ACTION_GET_CONTENT);
//            startActivityForResult(Intent.createChooser(intent, "Select File"), Pick_gallery);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Capture_camera) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                 bm = BitmapFactory.decodeFile(outuri.getPath(), options);
                //bm = (Bitmap) data.getExtras().get("data");
            } else if (requestCode == Pick_gallery) {
                choosedFile = new File(data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH));
                Log.d("tag", "onActivityResult: "+choosedFile.getAbsolutePath());

//                try {
//                    bm = MediaStore.Images.Media.getBitmap(Files.this.getContentResolver(), data.getData());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
            }
                uploadFile();
        }
    }

    private void uploadFile() {
        CommonUtills.progresshow(Files.this);
        MultipartBody.Part fileToUpload = null;
        if (bm != null) {
            File file = new File(CommonUtills.getRealPathFromURI(Files.this, CommonUtills.getImageUri(Files.this, bm)));
            RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            fileToUpload = MultipartBody.Part.createFormData("user_file", file.getName(), mFile);
        }
        else {
            RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), choosedFile);
            fileToUpload = MultipartBody.Part.createFormData("user_file", choosedFile.getName(), mFile);
        }
        RequestBody folderid = RequestBody.create(MediaType.parse("text/plain"), folder_id);
        service.uploadFile(fileToUpload, folderid).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (folderdata)
                        setFolderData();
                    else
                        setData();
                    CommonUtills.alertDialog(Files.this, "Files uploaded Successfully");
                } else
                    CommonUtills.alertDialog(Files.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(Files.this, "Something went Wrong Please try again");
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        hideSearch();
        if (menuItem.getItemId() == R.id.files) {
            add_iv.setVisibility(View.VISIBLE);
            changeTab(true, false, false);
            setData();
        } else if (menuItem.getItemId() == R.id.shared) {
            add_iv.setVisibility(View.GONE);
            changeTab(false, false, true);
            setSharedData();
        } else if (menuItem.getItemId() == R.id.recent) {
            add_iv.setVisibility(View.GONE);
            changeTab(false, true, false);
            setrecentData();
        }
        return true;
    }

    private void changeTab(boolean b, boolean b1, boolean b2) {
        all = b;
        recent = b1;
        shared = b2;
    }


    private void initCreate(int option, int create) {
        option_ln.setVisibility(option);
        create_rl.setVisibility(create);
    }

    private void createFolder() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("media_type", "folder");
        jsonObject.addProperty("folder_name", name_tv.getText().toString().trim());
        jsonObject.addProperty("user_id", new Sessionmanager(Files.this).getToken().userId);
        CommonUtills.progresshow(Files.this);
        service.createFolder(name_tv.getText().toString().trim()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                CommonUtills.progrescancel();
                sheetBehavior.dismiss();
                if (response.body().get("success").getAsBoolean()) {
                    setData();
                    CommonUtills.alertDialog(Files.this,"Folder Created Successfully");
                } else
                    CommonUtills.alertDialog(Files.this, response.body().get("message").getAsString());
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(Files.this, "Something went Wrong Please try again");
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_iv) {
            v = getLayoutInflater().inflate(R.layout.files_bottomlayout, null);
            sheetBehavior.setContentView(v);
            close = sheetBehavior.findViewById(R.id.close);
            cam_ln = sheetBehavior.findViewById(R.id.cam_ln);
            createFolder_ln = sheetBehavior.findViewById(R.id.createFolder_ln);
            option_ln = sheetBehavior.findViewById(R.id.option_ln);
            create_rl = sheetBehavior.findViewById(R.id.create_rl);
            upload_ln = sheetBehavior.findViewById(R.id.upload_ln);
            create_tv = sheetBehavior.findViewById(R.id.create_tv);
            cancel_tv = sheetBehavior.findViewById(R.id.cancel_tv);
            name_tv = sheetBehavior.findViewById(R.id.name_tv);
            if (folderdata)
                createFolder_ln.setVisibility(View.GONE);
            close.setOnClickListener(this);
            cam_ln.setOnClickListener(this);
            createFolder_ln.setOnClickListener(this);
            upload_ln.setOnClickListener(this);
            create_tv.setOnClickListener(this);
            toggle_iv.setOnClickListener(this);
            cancel_tv.setOnClickListener(this);
            sheetBehavior.show();
        } else if (v.getId() == R.id.close)
            sheetBehavior.dismiss();
        else if (v.getId() == R.id.profile_iv)
            startActivity(new Intent(Files.this, ApplicationPreferencesActivity.class));
        else if (v.getId() == R.id.cam_ln) {
            items = "Take Photo";
            sheetBehavior.dismiss();
            checkPermission();
        } else if (v.getId() == R.id.upload_ln) {
            items = "Library";
            sheetBehavior.dismiss();
            checkPermission();
        } else if (v.getId() == R.id.createFolder_ln) {
            initCreate(View.GONE, View.VISIBLE);
        } else if (v.getId() == R.id.cancel_tv) {
            initCreate(View.VISIBLE, View.GONE);
        } else if (v.getId() == R.id.create_tv) {
            createFolder();
            initCreate(View.VISIBLE, View.GONE);
        } else if (v.getId() == R.id.toggle_iv) {
            hideSearch();
            toggle_iv.setImageResource(isList ? R.drawable.grid : R.drawable.list);
            isList = !isList;
            search_et.setVisibility(View.GONE);
            if (all) {
                if (!folderdata)
                    setData();
                else setFolderData();
            } else if (recent)
                setrecentData();
            else if (shared)
                setSharedData();
        } else if (v.getId() == R.id.search_iv) {
            CommonUtills.hideSoftKeyboard(Files.this);
            if (search_et.getVisibility() == View.GONE) {
                Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_from_top);
                search_et.setVisibility(View.VISIBLE);
                search_et.startAnimation(slideUp);
                search_iv.setImageResource(R.drawable.searchclose);
            } else hideSearch();
        }
    }

    private void hideSearch() {
        search_et.setVisibility(View.GONE);
        search_iv.setImageResource(R.drawable.search);
        search_et.setText("");
    }

    public void setData() {
        filesPojos.clear();
        Folderlist.clear();
        CommonUtills.progresshow(Files.this);
        service.files().enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().get("success").getAsBoolean()) {
                        if (response.body().get("data").getAsJsonObject().get("folders").getAsJsonArray().size()>0 || response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().size()>0) {

                            try {
                                if (response.body().get("data").getAsJsonObject().has("folders")) {
                                    folderdata = false;
                                    folder_id = "";
                                    FilesPojo filesPojo = new FilesPojo();
                                    filesPojo.type = 0;
                                    filesPojo.name = "Root";
                                    filesPojo.id = "";
                                    Folderlist.add(filesPojo);
                                    for (int i = 0; i < response.body().get("data").getAsJsonObject().get("folders").getAsJsonArray().size(); i++) {
                                        filesPojo = new FilesPojo();
                                        filesPojo.type = 0;
                                        filesPojo.name = response.body().get("data").getAsJsonObject().get("folders").getAsJsonArray().get(i).getAsJsonObject().get("title").getAsString();
                                        filesPojo.id = response.body().get("data").getAsJsonObject().get("folders").getAsJsonArray().get(i).getAsJsonObject().get("id").getAsString();
                                        filesPojo.date = response.body().get("data").getAsJsonObject().get("folders").getAsJsonArray().get(i).getAsJsonObject().get("created_at").getAsString();
                                        filesPojos.add(filesPojo);
                                        Folderlist.add(filesPojo);
                                    }
                                }
                                if (response.body().get("data").getAsJsonObject().has("files")) {
                                    for (int i = 0; i < response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().size(); i++) {
                                        FilesPojo filesPojo = new FilesPojo();
                                        filesPojo.type = 1;
                                        filesPojo.name = response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("title").getAsString();
                                        filesPojo.id = response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("id").getAsString();
                                        filesPojo.date = response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("created_at").getAsString();
                                        if(!response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("path").isJsonNull())
                                        filesPojo.url = response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("path").getAsString();
                                        if(!response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("extension").isJsonNull())
                                            filesPojo.extension = response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("extension").getAsString();
                                        filesPojos.add(filesPojo);
                                    }
                                }
                                files_rv.setVisibility(View.VISIBLE);
                                oops_tv.setVisibility(View.GONE);
                                if (isList)
                                    files_rv.setLayoutManager(new LinearLayoutManager(Files.this));
                                else
                                    files_rv.setLayoutManager(new GridLayoutManager(Files.this, 3));
                                files_rv.setAdapter(new FilesAdapter(Files.this, toolbar, filesPojos, isList, sheetBehavior, Files.this, folderdata, all, recent, shared));

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            oops_tv.setText("No files found");
                            files_rv.setVisibility(View.GONE);
                            oops_tv.setVisibility(View.VISIBLE);
                        }
                    } else {
                        oops_tv.setText("No files found");
                        files_rv.setVisibility(View.GONE);
                        oops_tv.setVisibility(View.VISIBLE);
                    }
                } else
                    CommonUtills.alertDialog(Files.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(Files.this, "Something went Wrong Please try again");
            }
        });
    }

    public void setSharedData() {
        filesPojos.clear();
        Folderlist.clear();
        CommonUtills.progresshow(Files.this);
        service.sharedFiles().enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().get("success").getAsBoolean()) {
                        if (response.body().get("data").getAsJsonObject().get("folders").getAsJsonArray().size()>0 || response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().size()>0) {
                            try {
                                if (response.body().get("data").getAsJsonObject().has("folders")) {
                                    folderdata = false;
                                    folder_id = "";
                                    FilesPojo filesPojo = new FilesPojo();
                                    filesPojo.type = 0;
                                    filesPojo.name = "Root";
                                    filesPojo.id = "";
                                    Folderlist.add(filesPojo);
                                    for (int i = 0; i < response.body().get("data").getAsJsonObject().get("folders").getAsJsonArray().size(); i++) {
                                        filesPojo = new FilesPojo();
                                        filesPojo.type = 0;
                                        filesPojo.name = response.body().get("data").getAsJsonObject().get("folders").getAsJsonArray().get(i).getAsJsonObject().get("title").getAsString();
                                        filesPojo.id = response.body().get("data").getAsJsonObject().get("folders").getAsJsonArray().get(i).getAsJsonObject().get("id").getAsString();
                                        filesPojo.date = response.body().get("data").getAsJsonObject().get("folders").getAsJsonArray().get(i).getAsJsonObject().get("created_at").getAsString();
                                        filesPojos.add(filesPojo);
                                        Folderlist.add(filesPojo);
                                    }
                                }
                                if (response.body().get("data").getAsJsonObject().has("files")) {
                                    for (int i = 0; i < response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().size(); i++) {
                                        FilesPojo filesPojo = new FilesPojo();
                                        filesPojo.type = 1;
                                        filesPojo.name = response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("title").getAsString();
                                        filesPojo.id = response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("id").getAsString();
                                        filesPojo.date = response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("created_at").getAsString();
                                        if(!response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("path").isJsonNull())
                                            filesPojo.url = response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("path").getAsString();
                                        if(!response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("extension").isJsonNull())
                                            filesPojo.extension = response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("extension").getAsString();
                                        filesPojos.add(filesPojo);
                                    }
                                }
                                files_rv.setVisibility(View.VISIBLE);
                                oops_tv.setVisibility(View.GONE);
                                if (isList)
                                    files_rv.setLayoutManager(new LinearLayoutManager(Files.this));
                                else
                                    files_rv.setLayoutManager(new GridLayoutManager(Files.this, 3));
                                files_rv.setAdapter(new FilesAdapter(Files.this, toolbar, filesPojos, isList, sheetBehavior, Files.this, folderdata, all, recent, shared));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            oops_tv.setText("No files found");
                            files_rv.setVisibility(View.GONE);
                            oops_tv.setVisibility(View.VISIBLE);
                        }
                    } else {
                        oops_tv.setText("No files found");
                        files_rv.setVisibility(View.GONE);
                        oops_tv.setVisibility(View.VISIBLE);
                    }
                } else
                    CommonUtills.alertDialog(Files.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(Files.this, "Something went Wrong Please try again");
            }
        });
    }

    public void setFolderData() {
        CommonUtills.progresshow(Files.this);
        service.folderdata(folder_id).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    folderdata = true;
                    if (response.body().get("success").getAsBoolean()) {
                        if (response.body().get("data").getAsJsonArray().size()>0) {
                            filesPojos.clear();
                            for (int i = 0; i < response.body().get("data").getAsJsonArray().size(); i++) {
                                FilesPojo filesPojo = new FilesPojo();
                                filesPojo.type = 1;
                                filesPojo.name = response.body().get("data").getAsJsonArray().get(i).getAsJsonObject().get("title").getAsString();
                                filesPojo.id = response.body().get("data").getAsJsonArray().get(i).getAsJsonObject().get("id").getAsString();
                                filesPojo.date = response.body().get("data").getAsJsonArray().get(i).getAsJsonObject().get("created_at").getAsString();
                                if(!response.body().get("data").getAsJsonArray().get(i).getAsJsonObject().get("path").isJsonNull())
                                    filesPojo.url = response.body().get("data").getAsJsonArray().get(i).getAsJsonObject().get("path").getAsString();
                                if(!response.body().get("data").getAsJsonArray().get(i).getAsJsonObject().get("extension").isJsonNull())
                                    filesPojo.extension = response.body().get("data").getAsJsonArray().get(i).getAsJsonObject().get("extension").getAsString();
                                filesPojos.add(filesPojo);
                            }
                            files_rv.setVisibility(View.VISIBLE);
                            oops_tv.setVisibility(View.GONE);
                            if (isList)
                                files_rv.setLayoutManager(new LinearLayoutManager(Files.this));
                            else files_rv.setLayoutManager(new GridLayoutManager(Files.this, 3));
                            files_rv.setAdapter(new FilesAdapter(Files.this, toolbar, filesPojos, isList, sheetBehavior, Files.this, folderdata, all, recent, shared));
                        } else {
                            files_rv.setVisibility(View.GONE);
                            oops_tv.setText("Folder is Empty");
                            oops_tv.setVisibility(View.VISIBLE);
                        }
                    } else {
                        files_rv.setVisibility(View.GONE);
                        oops_tv.setText("Folder is Empty");
                        oops_tv.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(Files.this, "Something went Wrong Please try again");
            }
        });
    }

    public void setrecentData() {
        CommonUtills.progresshow(Files.this);
        service.recenFiles().enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().get("success").getAsBoolean()) {
                        if (response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().size()>0) {
                            filesPojos.clear();
                            for (int i = 0; i < response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().size(); i++) {
                                FilesPojo filesPojo = new FilesPojo();
                                filesPojo.type = 1;
                                filesPojo.name = response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("title").getAsString();
                                filesPojo.id = response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("id").getAsString();
                                filesPojo.date = response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("created_at").getAsString();
                                if(!response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("path").isJsonNull())
                                filesPojo.url = response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("path").getAsString();
                                if(!response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("extension").isJsonNull())
                                    filesPojo.extension = response.body().get("data").getAsJsonObject().get("files").getAsJsonArray().get(i).getAsJsonObject().get("extension").getAsString();
                                filesPojos.add(filesPojo);
                            }
                            files_rv.setVisibility(View.VISIBLE);
                            oops_tv.setVisibility(View.GONE);
                            if (isList)
                                files_rv.setLayoutManager(new LinearLayoutManager(Files.this));
                            else files_rv.setLayoutManager(new GridLayoutManager(Files.this, 3));
                            files_rv.setAdapter(new FilesAdapter(Files.this, toolbar, filesPojos, isList, sheetBehavior, Files.this, folderdata, all, recent, shared));
                        } else {
                            files_rv.setVisibility(View.GONE);
                            oops_tv.setText("No Files Found");
                            oops_tv.setVisibility(View.VISIBLE);
                        }
                    } else {
                        files_rv.setVisibility(View.GONE);
                        oops_tv.setText("No Files Found");
                        oops_tv.setVisibility(View.VISIBLE);
                    }
                } else
                    CommonUtills.alertDialog(Files.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(Files.this, "Something went Wrong Please try again");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (new Sessionmanager(this).getToken().data.profileImg!=null)
            Glide.with(this).load(new Sessionmanager(this).getToken().data.profileImg).into(profile_iv);
        add_iv.setVisibility(View.VISIBLE);
        search_iv.setVisibility(View.VISIBLE);
        title_tv.setText("Files");
    }

    @Override
    public void click(String id) {
        folder_id = id;
        setFolderData();
    }

    @Override
    public void onBackPressed() {
        if (folderdata) {
            if (!shared)
                setData();
            else setSharedData();

        } else
            super.onBackPressed();

    }
}