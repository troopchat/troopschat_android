package org.chat.troopschat.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;

import org.chat.troopschat.Adapter.ChecklistAdapter;
import org.chat.troopschat.Adapter.NotesAdapter;
import org.chat.troopschat.ApplicationPreferencesActivity;
import org.chat.troopschat.Pojo.CheckListPojo;
import org.chat.troopschat.Pojo.NotesPojo;
import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;
import org.chat.troopschat.Utils.CustomRecyclerView;
import org.chat.troopschat.Utils.Sessionmanager;
import org.chat.troopschat.Utils.rest.ApiClient;
import org.chat.troopschat.Utils.rest.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Notes extends AppCompatActivity implements View.OnClickListener {
    public ApiInterface service;
    ImageView profile_iv, delete_iv, add_iv, search_iv, more_iv, close;
    EditText search_et, note_et, name_tv, title_et;
    CustomRecyclerView files_rv;
    TextView oops_tv, headtitle_tv, create_tv, title_tv;
    LinearLayout create_rl, noteselect_ln, checklisttrash_ln, trash_ln, chkselect_ln, option_ln;
    BottomSheetDialog sheetBehavior;
    LinearLayout toolbar;
    List<NotesPojo.Datum> list = new ArrayList<>();
    List<CheckListPojo.Datum> checklists = new ArrayList<>();
    boolean trash, checktrash, checklist;

    public void setTrashData() {
        CommonUtills.progresshow(Notes.this);
        service.trashnotes().enqueue(new Callback<NotesPojo>() {
            @Override
            public void onResponse(@NonNull Call<NotesPojo> call, @NonNull Response<NotesPojo> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().status) {
                        if (response.body().data != null && response.body().data.size()>0) {
                            sheetBehavior.dismiss();
                            list = response.body().data;
                            files_rv.setVisibility(View.VISIBLE);
                            oops_tv.setVisibility(View.GONE);
                            files_rv.setAdapter(new NotesAdapter(Notes.this, toolbar, response.body().data, trash));
                        } else {
                            oops_tv.setText("No Notes found");
                            files_rv.setVisibility(View.GONE);
                            oops_tv.setVisibility(View.VISIBLE);
                            delete_iv.setVisibility(View.GONE);
                        }
                    } else {
                        oops_tv.setText("No Notes found");
                        files_rv.setVisibility(View.GONE);
                        oops_tv.setVisibility(View.VISIBLE);
                        delete_iv.setVisibility(View.GONE);
                    }
                } else
                    CommonUtills.alertDialog(Notes.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(@NonNull Call<NotesPojo> call, @NonNull Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(Notes.this, "Something went Wrong Please try again");
            }
        });

    }

    public void setcheckTrashData() {
        CommonUtills.progresshow(Notes.this);
        service.trashchecklist().enqueue(new Callback<CheckListPojo>() {
            @Override
            public void onResponse(@NonNull Call<CheckListPojo> call, @NonNull Response<CheckListPojo> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().status) {
                        if (response.body().data != null && response.body().data.size()>0) {
                            sheetBehavior.dismiss();
                            checklists = response.body().data;
                            files_rv.setVisibility(View.VISIBLE);
                            oops_tv.setVisibility(View.GONE);
                            files_rv.setAdapter(new ChecklistAdapter(Notes.this, toolbar, response.body().data, checktrash));
                        } else {
                            oops_tv.setText("No Checklist found");
                            files_rv.setVisibility(View.GONE);
                            oops_tv.setVisibility(View.VISIBLE);
                            delete_iv.setVisibility(View.GONE);
                        }
                    } else {
                        oops_tv.setText("No Checklist found");
                        files_rv.setVisibility(View.GONE);
                        oops_tv.setVisibility(View.VISIBLE);
                        delete_iv.setVisibility(View.GONE);
                    }
                } else
                    CommonUtills.alertDialog(Notes.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(@NonNull Call<CheckListPojo> call, @NonNull Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(Notes.this, "Something went Wrong Please try again");
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        service = ApiClient.createService(ApiInterface.class, this,new Sessionmanager(this).getToken().userId);
        sheetBehavior = new BottomSheetDialog(this);
        setContentView(R.layout.activity_notes);
        initViews();
    }

    private void initViews() {
        profile_iv = findViewById(R.id.profile_iv);
        add_iv = findViewById(R.id.add_iv);
        toolbar = findViewById(R.id.toolbar);
        headtitle_tv = findViewById(R.id.headtitle_tv);
        search_iv = findViewById(R.id.search_iv);
        more_iv = findViewById(R.id.more_iv);
        search_et = findViewById(R.id.search_et);
        delete_iv = findViewById(R.id.delete_iv);
        files_rv = findViewById(R.id.files_rv);
        oops_tv = findViewById(R.id.oops_tv);
        files_rv.setLayoutManager(new LinearLayoutManager(Notes.this));
        initClick();
    }

    private void initClick() {
        add_iv.setOnClickListener(this);
        search_iv.setOnClickListener(this);
        delete_iv.setOnClickListener(this);
        more_iv.setOnClickListener(this);
        profile_iv.setOnClickListener(this);
        search_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (checklist)
                    checkfilter(editable.toString());
                else
                    filter(editable.toString());
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_iv) {
            v = getLayoutInflater().inflate(R.layout.noteadd_layout, null);
            sheetBehavior.setContentView(v);
            close = sheetBehavior.findViewById(R.id.close);
            option_ln = sheetBehavior.findViewById(R.id.option_ln);
            title_et = sheetBehavior.findViewById(R.id.title_et);
            title_tv = sheetBehavior.findViewById(R.id.title_tv);
            note_et = sheetBehavior.findViewById(R.id.note_et);
            note_et.setVisibility(View.GONE);
            if (checklist) {
                title_tv.setText("Add Checklist");
            }
            create_rl = sheetBehavior.findViewById(R.id.create_rl);
            create_tv = sheetBehavior.findViewById(R.id.create_tv);
            close.setOnClickListener(this);
            create_tv.setOnClickListener(this);
            sheetBehavior.show();
        } else if (v.getId() == R.id.delete_iv) {
            deleteall();
        } else if (v.getId() == R.id.more_iv) {
            v = getLayoutInflater().inflate(R.layout.notemore_layout, null);
            sheetBehavior.setContentView(v);
            close = sheetBehavior.findViewById(R.id.close);
            noteselect_ln = sheetBehavior.findViewById(R.id.noteselect_ln);
            option_ln = sheetBehavior.findViewById(R.id.option_ln);
            chkselect_ln = sheetBehavior.findViewById(R.id.chkselect_ln);
            trash_ln = sheetBehavior.findViewById(R.id.notestrash_ln);
            checklisttrash_ln = sheetBehavior.findViewById(R.id.checklisttrash_ln);
            name_tv = sheetBehavior.findViewById(R.id.name_tv);
            close.setOnClickListener(this);
            noteselect_ln.setOnClickListener(this);
            checklisttrash_ln.setOnClickListener(this);
            chkselect_ln.setOnClickListener(this);
            trash_ln.setOnClickListener(this);
            if (sheetBehavior.isShowing())
                sheetBehavior.dismiss();
            sheetBehavior.show();
        } else if (v.getId() == R.id.noteselect_ln) {
            hideSearch();
            trash = false;
            checklist = false;
            delete_iv.setVisibility(View.GONE);
            headtitle_tv.setText("Notes");
            add_iv.setVisibility(View.VISIBLE);
            sheetBehavior.dismiss();
            setData();
        } else if (v.getId() == R.id.close)
            sheetBehavior.dismiss();
        else if (v.getId() == R.id.create_tv) {
            if (title_et.getText().toString().trim().isEmpty()) {
                title_et.setError("Title is Required");
                title_et.requestFocus();
            } else {
                if (!checklist)
                    createNote();
                else
                    createchecklist();
            }
        } else if (v.getId() == R.id.profile_iv)
            startActivity(new Intent(Notes.this, ApplicationPreferencesActivity.class));
        else if (v.getId() == R.id.search_iv) {
            CommonUtills.hideSoftKeyboard(Notes.this);
            if (search_et.getVisibility() == View.GONE) {
                Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_from_top);
                search_et.setVisibility(View.VISIBLE);
                search_et.startAnimation(slideUp);
                search_iv.setImageResource(R.drawable.searchclose);
            } else
                hideSearch();

        } else if (v.getId() == R.id.notestrash_ln) {
            hideSearch();
            trash = true;
            delete_iv.setVisibility(View.VISIBLE);
            headtitle_tv.setText("Notes (Trash)");
            add_iv.setVisibility(View.GONE);
            setTrashData();
            sheetBehavior.dismiss();
        } else if (v.getId() == R.id.checklisttrash_ln) {
            hideSearch();
            checktrash = true;
            delete_iv.setVisibility(View.VISIBLE);
            headtitle_tv.setText("Checklist (Trash)");
            add_iv.setVisibility(View.GONE);
            setcheckTrashData();
            sheetBehavior.dismiss();
        } else if (v.getId() == R.id.chkselect_ln) {
            hideSearch();
            checklist = true;
            delete_iv.setVisibility(View.GONE);
            checktrash = false;
            headtitle_tv.setText("Checklist");
            add_iv.setVisibility(View.VISIBLE);
            setChecklistData();
            sheetBehavior.dismiss();
        }
    }

    private void hideSearch() {
        search_et.setVisibility(View.GONE);
        search_iv.setImageResource(R.drawable.search);
        search_et.setText("");
    }

    private void deleteall() {
        final Call<JsonObject> call;
        if (trash)
            call = service.emptynotestrash();
        else
            call = service.emptychecktrash();
        AlertDialog.Builder builder = new AlertDialog.Builder(Notes.this);
        builder.setMessage("Are you Sure?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                        CommonUtills.progrescancel();
                        if (response.body() != null) {
                            if (response.body().get("success").getAsBoolean()) {
                                 if (trash)
                                    setTrashData();
                                else if (checktrash)
                                    setcheckTrashData();
                            }
                        } else
                            CommonUtills.alertDialog(Notes.this, "Something went Wrong Please try again");
                    }

                    @Override
                    public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                        CommonUtills.progrescancel();
                        CommonUtills.alertDialog(Notes.this, "Something went Wrong Please try again");
                    }
                });
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void createchecklist() {
        CommonUtills.progresshow(Notes.this);
        service.createChecklist(title_et.getText().toString().trim(),
                "#FDF4B3", 0,0).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().get("success").getAsBoolean()) {
                        CommonUtills.alertDialog(Notes.this, "Checklist created successfully");
                        sheetBehavior.dismiss();
                        setChecklistData();
                    }
                } else
                    CommonUtills.alertDialog(Notes.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(Notes.this, "Something went Wrong Please try again");
            }
        });
    }

    public void setChecklistData() {
        CommonUtills.progresshow(Notes.this);
        service.checklist().enqueue(new Callback<CheckListPojo>() {
            @Override
            public void onResponse(Call<CheckListPojo> call, Response<CheckListPojo> response) {
                CommonUtills.progrescancel();
                sheetBehavior.dismiss();
                if (response.body() != null) {
                    if (response.body().status) {
                        if (response.body().data != null && response.body().data.size()>0) {
                            sheetBehavior.dismiss();
                            checklists = response.body().data;
                            files_rv.setVisibility(View.VISIBLE);
                            oops_tv.setVisibility(View.GONE);
                            files_rv.setAdapter(new ChecklistAdapter(Notes.this, toolbar, response.body().data, checktrash));
                        } else {
                            oops_tv.setText("No Checklist found");
                            files_rv.setVisibility(View.GONE);
                            oops_tv.setVisibility(View.VISIBLE);
                        }
                    } else {
                        oops_tv.setText("No Checklist found");
                        files_rv.setVisibility(View.GONE);
                        oops_tv.setVisibility(View.VISIBLE);
                    }
                } else
                    CommonUtills.alertDialog(Notes.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(Call<CheckListPojo> call, Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(Notes.this, "Something went Wrong Please try again");
            }
        });
    }


    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<NotesPojo.Datum> filterdNames = new ArrayList<>();

        //looping through existing elements
        for (int i = 0; i < list.size(); i++) {
            //if the existing elements contains the search input
            if (list.get(i).title.toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(list.get(i));
            }
        }
        files_rv.setAdapter(new NotesAdapter(Notes.this, toolbar, filterdNames, trash));
    }


    private void checkfilter(String text) {
        //new array list that will hold the filtered data
        ArrayList<CheckListPojo.Datum> filterdNames = new ArrayList<>();

        //looping through existing elements
        for (int i = 0; i < checklists.size(); i++) {
            //if the existing elements contains the search input
            if (checklists.get(i).checkListTitle.toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(checklists.get(i));
            }
        }
        files_rv.setAdapter(new ChecklistAdapter(Notes.this, toolbar, filterdNames, checktrash));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (new Sessionmanager(this).getToken().data.profileImg!=null)
            Glide.with(this).load(new Sessionmanager(this).getToken().data.profileImg).into(profile_iv);
        if (checklist)
            setChecklistData();
        else if (trash)
            setTrashData();
        else if (checktrash)
            setcheckTrashData();
        else
            setData();
    }

    public void setData() {
        CommonUtills.progresshow(Notes.this);
        service.notes().enqueue(new Callback<NotesPojo>() {
            @Override
            public void onResponse(Call<NotesPojo> call, Response<NotesPojo> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().status) {
                        if (response.body().data != null && response.body().data.size()>0) {
                            sheetBehavior.dismiss();
                            list = response.body().data;
                            files_rv.setVisibility(View.VISIBLE);
                            oops_tv.setVisibility(View.GONE);
                            files_rv.setAdapter(new NotesAdapter(Notes.this, toolbar, response.body().data, trash));
                        } else {
                            oops_tv.setText("No Notes found");
                            files_rv.setVisibility(View.GONE);
                            oops_tv.setVisibility(View.VISIBLE);
                        }
                    } else {
                        oops_tv.setText("No Notes found");
                        files_rv.setVisibility(View.GONE);
                        oops_tv.setVisibility(View.VISIBLE);
                    }
                } else
                    CommonUtills.alertDialog(Notes.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(Call<NotesPojo> call, Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(Notes.this, "Something went Wrong Please try again");
            }
        });
    }

    public void createNote() {
        CommonUtills.progresshow(Notes.this);
        service.createNote(title_et.getText().toString().trim(),
                "", "#FDF4B3", 0, 0).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().get("success").getAsBoolean()) {
                        sheetBehavior.dismiss();
                        setData();
                    }
                    CommonUtills.alertDialog(Notes.this, "Note created successfully");
                } else
                    CommonUtills.alertDialog(Notes.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(Notes.this, "Something went Wrong Please try again");
            }
        });
    }
}
