package org.chat.troopschat.Activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import org.chat.troopschat.ConversationListActivity;
import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;
import org.chat.troopschat.Utils.Sessionmanager;

public class SplashActivity extends AppCompatActivity {
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CommonUtills.hidestatusbar(this);
        setContentView(R.layout.activity_splash);
        handlerToLoginActivity();
    }

    public void handlerToLoginActivity() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, new Sessionmanager(SplashActivity.this).isLoggedIn() ? ConversationListActivity.class : LoginActivity.class));
               // startActivity(new Intent(SplashActivity.this, new Sessionmanager(SplashActivity.this).isLoggedIn() ? MainActivity.class : LoginActivity.class));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    finishAffinity();
                } else finish();
            }
        }, 3000);
    }
}