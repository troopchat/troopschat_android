package org.chat.troopschat.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.chat.troopschat.Adapter.ChecklistItemAdapter;
import org.chat.troopschat.Pojo.CheckListItemPojo;
import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;
import org.chat.troopschat.Utils.Sessionmanager;
import org.chat.troopschat.Utils.rest.ApiClient;
import org.chat.troopschat.Utils.rest.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckListDetail extends AppCompatActivity implements View.OnClickListener {
    public ApiInterface service;
    ImageView back, edit_iv, add_iv, close, more_iv, save;
    TextView title_tv, oops_tv, create_tv, date_tv, check_tv, lock_tv;
    EditText note_et, title_et;
    LinearLayout main_ln, send_ln, chkselect_ln, lock_ln, reminder_ln, trash_ln;
    boolean isedit;
    RecyclerView items_rv;
    List<String> colors = new ArrayList<>();
    BottomSheetDialog sheetBehavior;
    LinearLayout toolbar;
    List<CheckListItemPojo.Datum> data = new ArrayList<>();
    boolean lock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sheetBehavior = new BottomSheetDialog(this);
        service = ApiClient.createService(ApiInterface.class, this, new Sessionmanager(this).getToken().userId);
        setContentView(R.layout.activity_check_list_detail);
        initviews();
    }

    private void initviews() {
        back = findViewById(R.id.back);
        toolbar = findViewById(R.id.toolbar);
        oops_tv = findViewById(R.id.oops_tv);
        edit_iv = findViewById(R.id.edit_iv);
        more_iv = findViewById(R.id.more_iv);
        items_rv = findViewById(R.id.items_rv);
        save = findViewById(R.id.save);
        title_tv = findViewById(R.id.title_tv);
        date_tv = findViewById(R.id.date_tv);
        main_ln = findViewById(R.id.main_ln);
        title_et = findViewById(R.id.title_et);
        add_iv = findViewById(R.id.add_iv);
        colors.add("#ff6900");
        colors.add("#23c0e9");
        colors.add("#ffca00");
        colors.add("#E91E63");
        colors.add("#FDF4B3");
        lock = getIntent().getBooleanExtra("lock", false);
        items_rv.setLayoutManager(new LinearLayoutManager(CheckListDetail.this));
        initData();
        getItems();
        initClick();
    }

    public void getItems() {
        CommonUtills.progresshow(CheckListDetail.this);
        service.Checklistitems(getIntent().getStringExtra("id")).enqueue(new Callback<CheckListItemPojo>() {
            @Override
            public void onResponse(@NonNull Call<CheckListItemPojo> call, @NonNull Response<CheckListItemPojo> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().status) {
                        data = response.body().data;
                        items_rv.setAdapter(new ChecklistItemAdapter(CheckListDetail.this, toolbar, response.body().data));
                        items_rv.setVisibility(View.VISIBLE);
                        oops_tv.setVisibility(View.GONE);
                    } else {
                        items_rv.setVisibility(View.GONE);
                        oops_tv.setVisibility(View.VISIBLE);
                    }
                } else
                    CommonUtills.alertDialog(CheckListDetail.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(@NonNull Call<CheckListItemPojo> call, @NonNull Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(CheckListDetail.this, "Something went Wrong Please try again");
            }
        });
    }

    private void initClick() {
        back.setOnClickListener(this);
        save.setOnClickListener(this);
        edit_iv.setOnClickListener(this);
        more_iv.setOnClickListener(this);
        add_iv.setOnClickListener(this);
    }

    private void initData() {
        title_tv.setText(getIntent().getStringExtra("title"));
        title_et.setText(getIntent().getStringExtra("title"));
        date_tv.setText(getIntent().getStringExtra("date"));
        try {
            main_ln.setBackgroundColor(Color.parseColor(getIntent().getStringExtra("color")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.add_iv:
                v = getLayoutInflater().inflate(R.layout.noteadd_layout, null);
                sheetBehavior.setContentView(v);
                close = sheetBehavior.findViewById(R.id.close);
                EditText title_et = sheetBehavior.findViewById(R.id.title_et);
                note_et = sheetBehavior.findViewById(R.id.note_et);
                title_tv = sheetBehavior.findViewById(R.id.title_tv);
                title_tv.setText("Add Checklist");
                title_et.setVisibility(View.GONE);
                note_et.setHint("Enter Item");
                create_tv = sheetBehavior.findViewById(R.id.create_tv);
                close.setOnClickListener(this);
                create_tv.setOnClickListener(this);
                sheetBehavior.show();
                break;
            case R.id.create_tv:
                addItem();
                break;
            case R.id.edit_iv:
                if (isedit) {
                    CommonUtills.showDialog(CheckListDetail.this, colors, main_ln);
                } else {
                    isedit = true;
                    initEdit();
                }
                break;
            case R.id.more_iv:
                v = getLayoutInflater().inflate(R.layout.notedetailmore_layout, null);
                sheetBehavior.setContentView(v);
                close = sheetBehavior.findViewById(R.id.close);
                send_ln = sheetBehavior.findViewById(R.id.send_ln);
                chkselect_ln = sheetBehavior.findViewById(R.id.chkselect_ln);
                check_tv = sheetBehavior.findViewById(R.id.check_tv);
                lock_tv = sheetBehavior.findViewById(R.id.lock_tv);
                lock_tv.setText(lock ? "UnLock" : "Lock");
                chkselect_ln = sheetBehavior.findViewById(R.id.chkselect_ln);
                chkselect_ln.setVisibility(View.GONE);
                send_ln.setOnClickListener(this);                // reminder_ln = sheetBehavior.findViewById(R.id.reminder_ln);
                trash_ln = sheetBehavior.findViewById(R.id.trash_ln);
                trash_ln.setVisibility(lock ? View.GONE : View.VISIBLE);
                lock_ln = sheetBehavior.findViewById(R.id.lock_ln);
                lock_ln.setOnClickListener(this);
                close.setOnClickListener(this);
                chkselect_ln.setOnClickListener(this);
                //reminder_ln.setOnClickListener(this);
                trash_ln.setOnClickListener(this);
                sheetBehavior.show();
                break;
            case R.id.save:
                upDate();
                break;
            case R.id.send_ln:
                StringBuilder detai = null;
                for (int i = 0; i < data.size(); i++) {
                    if (detai == null) {
                        detai = new StringBuilder(i+1 + " " + data.get(i).itemTitle);
                    } else detai.append("\n").append(i+1).append(" ").append(data.get(i).itemTitle);
                }
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, detai.toString().trim());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                sheetBehavior.dismiss();
                break;
            case R.id.trash_ln:
                delete();
                break;
            case R.id.close:
                sheetBehavior.dismiss();
                break;
            case R.id.lock_ln:
                lock = !lock;
                upDate();
                break;
        }
    }

    private void addItem() {
        service.ChecklistItem(getIntent().getStringExtra("id"), note_et.getText().toString().trim(), 0).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().get("success").getAsBoolean()) {
                        CommonUtills.alertDialog(CheckListDetail.this, "Items created");
                        getItems();
                        sheetBehavior.dismiss();
                    }
                } else
                    CommonUtills.alertDialog(CheckListDetail.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(CheckListDetail.this, "Something went Wrong Please try again");
            }
        });
    }

    private void delete() {
        final ArrayList<String> id = new ArrayList<>();
        id.add(getIntent().getStringExtra("id"));
        AlertDialog.Builder builder = new AlertDialog.Builder(CheckListDetail.this);
        builder.setMessage("Are you Sure?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                service.trashchecklist(id).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                        CommonUtills.progrescancel();
                        if (response.body() != null) {
                            if (response.body().get("success").getAsBoolean()) {
                                sheetBehavior.dismiss();
                                finish();
                            }
                        } else
                            CommonUtills.alertDialog(CheckListDetail.this, "Something went Wrong Please try again");
                    }

                    @Override
                    public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                        CommonUtills.progrescancel();
                        CommonUtills.alertDialog(CheckListDetail.this, "Something went Wrong Please try again");
                    }
                });
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void initEdit() {
        title_et.setVisibility(isedit ? View.VISIBLE : View.GONE);
        title_tv.setVisibility(isedit ? View.GONE : View.VISIBLE);
        title_tv.setText(title_et.getText().toString());
        back.setVisibility(isedit ? View.GONE : View.VISIBLE);
        save.setVisibility(isedit ? View.VISIBLE : View.GONE);
        edit_iv.setImageResource(isedit ? R.drawable.color : R.drawable.editwhite);
        if (lock_tv != null) {
            lock_tv.setText(lock ? "UnLock" : "Lock");
        }
    }

    public void upDate() {
        if (title_et.getText().toString().trim().isEmpty()) {
            title_et.setError("Title is Required");
            title_et.requestFocus();
        } else {
            ColorDrawable viewColor = (ColorDrawable) main_ln.getBackground();
            int colorId = viewColor.getColor();
            String hexColor = String.format("#%06X", (0xFFFFFF & colorId));
            CommonUtills.progresshow(CheckListDetail.this);
            service.updateChecklist(getIntent().getStringExtra("id"), title_et.getText().toString().trim(), hexColor, lock ? 1 : 0, 0).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                    CommonUtills.progrescancel();
                    if (response.body() != null) {
                        if (response.body().get("success").getAsBoolean()) {
                            isedit = false;
                            initEdit();
                            sheetBehavior.dismiss();
                        }
                    } else
                        CommonUtills.alertDialog(CheckListDetail.this, "Something went Wrong Please try again");
                }

                @Override
                public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                    CommonUtills.progrescancel();
                    CommonUtills.alertDialog(CheckListDetail.this, "Something went Wrong Please try again");
                }
            });
        }
    }
}