package org.chat.troopschat.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;
import org.chat.troopschat.Utils.rest.ApiClient;
import org.chat.troopschat.Utils.rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUp extends AppCompatActivity implements View.OnClickListener {
    EditText name_et, email_et, pwd_et, cpwd_et;
    TextView loginbtn_tv, haveaccount_tv;
    JsonObject jsonObject = new JsonObject();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        CommonUtills.colorstatusbar(this);
        initViews();
    }

    private void initViews() {
        name_et = findViewById(R.id.name_et);
        email_et = findViewById(R.id.email_et);
        pwd_et = findViewById(R.id.pwd_et);
        cpwd_et = findViewById(R.id.cpwd_et);
        loginbtn_tv = findViewById(R.id.loginbtn_tv);
        haveaccount_tv = findViewById(R.id.haveaccount_tv);
        initClicks();
    }

    private void initClicks() {
        loginbtn_tv.setOnClickListener(this);
        haveaccount_tv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.haveaccount_tv:
                startActivity(new Intent(SignUp.this, LoginActivity.class));
                break;
            case R.id.loginbtn_tv:
                CommonUtills.hideSoftKeyboard(SignUp.this);

                if (!CommonUtills.isNetworkConnected(SignUp.this)) {
                    Toast.makeText(SignUp.this, getString(R.string.networkerror), Toast.LENGTH_SHORT).show();
                } else if (name_et.getText().toString().trim().equals("")) {
                    name_et.setError(getString(R.string.nameerror));
                    name_et.requestFocus();
                } else if (email_et.getText().toString().trim().equals("")) {
                    email_et.setError(getString(R.string.emailerror));
                    email_et.requestFocus();
                } else if (!Patterns.EMAIL_ADDRESS.matcher(email_et.getText().toString().trim()).matches()) {
                    email_et.setError(getString(R.string.emailaddresserror));
                    email_et.requestFocus();
                } else if (pwd_et.getText().toString().trim().length() < 6) {
                    pwd_et.setError(getString(R.string.passworderror));
                    pwd_et.requestFocus();
                } else if (!cpwd_et.getText().toString().trim().equals(pwd_et.getText().toString().trim())) {
                    cpwd_et.setError(getString(R.string.confirmpassworderror));
                    cpwd_et.requestFocus();
                } else
                    register();
                break;
        }
    }

    private void register() {
        jsonObject.addProperty("name", name_et.getText().toString());
        jsonObject.addProperty("email", email_et.getText().toString());
        jsonObject.addProperty("password", pwd_et.getText().toString());
        CommonUtills.progresshow(SignUp.this);
        ApiInterface service = ApiClient.createService(ApiInterface.class, SignUp.this);
        service.signup(jsonObject).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, final @NonNull Response<JsonObject> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SignUp.this);
                    builder.setMessage(response.body().has("error")?response.body().get("error").getAsString():"Registered Successfully").setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (!response.body().has("error")) {
                                Intent intent = new Intent(SignUp.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else
                    CommonUtills.alertDialog(SignUp.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(SignUp.this, "Something went Wrong Please try again");
            }
        });
    }
}
