package org.chat.troopschat.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.chat.troopschat.ConversationListActivity;
import org.chat.troopschat.Pojo.User;
import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;
import org.chat.troopschat.Utils.Sessionmanager;
import org.chat.troopschat.Utils.rest.ApiClient;
import org.chat.troopschat.Utils.rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText email_et, pwd_et;
    TextView loginbtn_tv, haveaccount_tv, forget_tv;
    JsonObject jsonObject = new JsonObject();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        CommonUtills.colorstatusbar(this);
        initViews();
    }

    private void initViews() {
        email_et = findViewById(R.id.email_et);
        pwd_et = findViewById(R.id.pwd_et);
        loginbtn_tv = findViewById(R.id.loginbtn_tv);
        haveaccount_tv = findViewById(R.id.haveaccount_tv);
        forget_tv = findViewById(R.id.forget_tv);
        initClicks();
    }

    private void initClicks() {
        forget_tv.setOnClickListener(this);
        loginbtn_tv.setOnClickListener(this);
        haveaccount_tv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.haveaccount_tv:
                startActivity(new Intent(LoginActivity.this, SignUp.class));
                break;
            case R.id.loginbtn_tv:
                CommonUtills.hideSoftKeyboard(LoginActivity.this);
                if (isValid())
                    login();
                break;
            case R.id.forget_tv:
                startActivity(new Intent(LoginActivity.this, ForgetActivity.class));
                break;
        }
    }


    private void login() {
        jsonObject.addProperty("email", email_et.getText().toString());
        jsonObject.addProperty("password", pwd_et.getText().toString());
        CommonUtills.progresshow(LoginActivity.this);
        ApiInterface service = ApiClient.createService(ApiInterface.class, LoginActivity.this);
        service.login(jsonObject).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {

                    if (response.body().error==null) {
                        new Sessionmanager(LoginActivity.this).setLogin(true, response.body());
                        startActivity(new Intent(LoginActivity.this, ConversationListActivity.class));
                        finish();
                    } else {
                        CommonUtills.alertDialog(LoginActivity.this, response.body().error);
                    }
                } else
                    CommonUtills.alertDialog(LoginActivity.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(LoginActivity.this, "Something went Wrong Please try again");
            }
        });
    }

    private boolean isValid() {
        if (!CommonUtills.isNetworkConnected(LoginActivity.this)) {
            Toast.makeText(LoginActivity.this, getString(R.string.networkerror), Toast.LENGTH_SHORT).show();
            return false;
        } else if (email_et.getText().toString().trim().equals("")) {
            email_et.setError(getString(R.string.emailerror));
            email_et.requestFocus();
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email_et.getText().toString().trim()).matches()) {
            email_et.setError(getString(R.string.emailaddresserror));
            email_et.requestFocus();
            return false;
        } else if (pwd_et.getText().toString().trim().length() < 6) {
            pwd_et.setError(getString(R.string.passworderror));
            pwd_et.requestFocus();
            return false;
        }
        return true;
    }
}
