package org.chat.troopschat.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;
import org.chat.troopschat.Utils.rest.ApiClient;
import org.chat.troopschat.Utils.rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetActivity extends AppCompatActivity {
    EditText email_et;
    TextView submit_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget);
        email_et = findViewById(R.id.email_et);
        submit_tv = findViewById(R.id.submit_tv);
        submit_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email_et.getText().toString().trim().equals("")) {
                    email_et.setError(getString(R.string.emailerror));
                    email_et.requestFocus();
                } else if (!Patterns.EMAIL_ADDRESS.matcher(email_et.getText().toString().trim()).matches()) {
                    email_et.setError(getString(R.string.emailaddresserror));
                    email_et.requestFocus();
                } else {
                    CommonUtills.progresshow(ForgetActivity.this);
                    ApiInterface service = ApiClient.createService(ApiInterface.class, ForgetActivity.this);
                    service.forget(email_et.getText().toString()).enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            CommonUtills.progrescancel();
                            if (response.body() != null) {
                                finish();
                                CommonUtills.alertDialog(ForgetActivity.this, response.body().get("message").getAsString());
                            } else
                                CommonUtills.alertDialog(ForgetActivity.this, "Something went Wrong Please try again");
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            CommonUtills.progrescancel();
                            CommonUtills.alertDialog(ForgetActivity.this, "Something went Wrong Please try again");
                        }
                    });
                }
            }
        });
    }
}
