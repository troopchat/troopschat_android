package org.chat.troopschat.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;
import org.chat.troopschat.Utils.rest.ApiClient;
import org.chat.troopschat.Utils.rest.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NoteDetail extends AppCompatActivity implements View.OnClickListener {
    ImageView back, edit_iv, close, more_iv, save;
    TextView title_tv, time_tv, date_tv, check_tv, lock_tv;
    EditText notedetail_et, title_et;
    LinearLayout main_ln, send_ln, chkselect_ln, lock_ln, reminder_ln, trash_ln;
    boolean isedit;
    List<String> colors = new ArrayList<>();
    BottomSheetDialog sheetBehavior;
    boolean lock, status;
    ApiInterface service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sheetBehavior = new BottomSheetDialog(this);
        service = ApiClient.createService(ApiInterface.class, NoteDetail.this);
        setContentView(R.layout.activity_note_detail);
        initviews();
    }

    private void initviews() {
        back = findViewById(R.id.back);
        edit_iv = findViewById(R.id.edit_iv);
        more_iv = findViewById(R.id.more_iv);
        time_tv = findViewById(R.id.time_tv);
        save = findViewById(R.id.save);
        title_tv = findViewById(R.id.title_tv);
        date_tv = findViewById(R.id.date_tv);
        main_ln = findViewById(R.id.main_ln);
        title_et = findViewById(R.id.title_et);
        notedetail_et = findViewById(R.id.notedetail_et);
        colors.add("#ff6900");
        colors.add("#23c0e9");
        colors.add("#ffca00");
        colors.add("#E91E63");
        colors.add("#FDF4B3");
        lock = getIntent().getBooleanExtra("lock", false);
        status = getIntent().getBooleanExtra("status", false);
        if (status) {
            title_tv.setPaintFlags(title_tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            notedetail_et.setTextColor(Color.GRAY);
        }
        initData();
        initClick();
    }

    private void initClick() {
        back.setOnClickListener(this);
        save.setOnClickListener(this);
        edit_iv.setOnClickListener(this);
        more_iv.setOnClickListener(this);
    }

    private void initData() {
        title_tv.setText(getIntent().getStringExtra("title"));
        title_et.setText(getIntent().getStringExtra("title"));
        notedetail_et.setText(getIntent().getStringExtra("note"));
        date_tv.setText(getIntent().getStringExtra("date"));
        main_ln.setBackgroundColor(Color.parseColor(getIntent().getStringExtra("color")));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.edit_iv:
                if (isedit) {
                    CommonUtills.showDialog(NoteDetail.this, colors, main_ln);
                } else {
                    isedit = true;
                    initEdit();
                }
                break;
            case R.id.more_iv:
                final BottomSheetDialog sheetBehavior = new BottomSheetDialog(NoteDetail.this);
                v = getLayoutInflater().inflate(R.layout.notedetailmore_layout, null);
                sheetBehavior.setContentView(v);
                close = sheetBehavior.findViewById(R.id.close);
                send_ln = sheetBehavior.findViewById(R.id.send_ln);
                chkselect_ln = sheetBehavior.findViewById(R.id.chkselect_ln);
                check_tv = sheetBehavior.findViewById(R.id.check_tv);
                lock_tv = sheetBehavior.findViewById(R.id.lock_tv);
                check_tv.setText(status ? "Uncheck" : "Check");
                lock_tv.setText(lock ? "UnLock" : "Lock");
                chkselect_ln = sheetBehavior.findViewById(R.id.chkselect_ln);
                // reminder_ln = sheetBehavior.findViewById(R.id.reminder_ln);
                trash_ln = sheetBehavior.findViewById(R.id.trash_ln);
                trash_ln.setVisibility(lock ? View.GONE : View.VISIBLE);
                lock_ln = sheetBehavior.findViewById(R.id.lock_ln);
                lock_ln.setOnClickListener(this);
                trash_ln.setOnClickListener(this);
                send_ln.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, notedetail_et.getText().toString().trim());
                        sendIntent.setType("text/plain");
                        startActivity(sendIntent);
                        sheetBehavior.dismiss();
                    }
                });
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sheetBehavior.dismiss();
                    }
                });
                lock_ln.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        lock = !lock;
                        upDate();
                        sheetBehavior.dismiss();
                    }
                });
                chkselect_ln.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        status = !status;
                        upDate();
                        sheetBehavior.dismiss();
                    }
                });
                sheetBehavior.show();
                break;
            case R.id.save:
                upDate();
                break;
            case R.id.trash_ln:
                delete();
                break;
        }
        sheetBehavior.dismiss();
    }

    private void delete() {
        final ArrayList<String> id = new ArrayList<String>();
        id.add(getIntent().getStringExtra("id"));
        AlertDialog.Builder builder = new AlertDialog.Builder(NoteDetail.this);
        builder.setMessage("Are you Sure?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                service.trashnote(id).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                        CommonUtills.progrescancel();
                        if (response.body() != null) {
                            if (response.body().get("success").getAsBoolean()) {
                                sheetBehavior.dismiss();
                                finish();
                            }
                        } else
                            CommonUtills.alertDialog(NoteDetail.this, "Something went Wrong Please try again");
                    }

                    @Override
                    public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                        CommonUtills.progrescancel();
                        CommonUtills.alertDialog(NoteDetail.this, "Something went Wrong Please try again");
                    }
                });
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void initEdit() {
        title_et.setVisibility(isedit ? View.VISIBLE : View.GONE);
        title_tv.setVisibility(isedit ? View.GONE : View.VISIBLE);
        title_tv.setText(title_et.getText().toString());
        notedetail_et.setEnabled(isedit);
        back.setVisibility(isedit ? View.GONE : View.VISIBLE);
        save.setVisibility(isedit ? View.VISIBLE : View.GONE);
        edit_iv.setImageResource(isedit ? R.drawable.color : R.drawable.editwhite);
        if (lock_tv != null) {
            check_tv.setText(status ? "Uncheck" : "Check");
            lock_tv.setText(lock ? "UnLock" : "Lock");
        }
    }

    public void upDate() {
        ColorDrawable viewColor = (ColorDrawable) main_ln.getBackground();
        int colorId = viewColor.getColor();
        String hexColor = String.format("#%06X", (0xFFFFFF & colorId));
        CommonUtills.progresshow(NoteDetail.this);
        service.updateNote(getIntent().getStringExtra("id"), title_et.getText().toString().trim(), notedetail_et.getText().toString(), hexColor, status?1:0, lock?1:0).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().get("success").getAsBoolean()) {
                        CommonUtills.alertDialog(NoteDetail.this, "Note updated successfully");
                        isedit = false;
                        if (status) {
                            title_tv.setPaintFlags(title_tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            notedetail_et.setTextColor(Color.GRAY);
                        } else
                            title_tv.setPaintFlags(title_tv.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));

                        initEdit();
                        sheetBehavior.dismiss();
                    }
                } else
                    CommonUtills.alertDialog(NoteDetail.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(NoteDetail.this, "Something went Wrong Please try again");
            }
        });
    }
}