package org.chat.troopschat.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;

import org.chat.troopschat.Adapter.RostersAdapter;
import org.chat.troopschat.ApplicationPreferencesActivity;
import org.chat.troopschat.Pojo.RosterPojo;
import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;
import org.chat.troopschat.Utils.CustomRecyclerView;
import org.chat.troopschat.Utils.Sessionmanager;
import org.chat.troopschat.Utils.rest.ApiClient;
import org.chat.troopschat.Utils.rest.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Rosters extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    public ApiInterface service;
    CustomRecyclerView files_rv;
    BottomNavigationView file_bottom;
    TextView title_tv, oops_tv, create_tv, search_et;
    ImageView add_iv, profile_iv,qrcode_iv, delete_iv, search_iv, close,qr_iv;
    LinearLayout bottom_sheet, option_ln, main_ln,create_rl;
    BottomSheetDialog sheetBehavior;
    View toolbar;
    EditText title_et, note_et;
    boolean all = true, trash, recent, shared;
    boolean isList = true;
    List<RosterPojo.Datum> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rosters);
        service = ApiClient.createService(ApiInterface.class, Rosters.this,new Sessionmanager(this).getToken().userId);
        initViews();
    }

    private void initViews() {
        files_rv = findViewById(R.id.files_rv);
        oops_tv = findViewById(R.id.oops_tv);
        toolbar = findViewById(R.id.toolbar);
        file_bottom = findViewById(R.id.file_bottom);
        title_tv = findViewById(R.id.title_tv);
        delete_iv = findViewById(R.id.delete_iv);
        qrcode_iv = findViewById(R.id.qrcode_iv);
        profile_iv = findViewById(R.id.profile_iv);
        main_ln = findViewById(R.id.main_ln);
        add_iv = findViewById(R.id.add_iv);
        bottom_sheet = findViewById(R.id.bottom_sheet);
        search_iv = findViewById(R.id.search_iv);
        search_et = findViewById(R.id.search_et);
        qr_iv = findViewById(R.id.qr_iv);
        sheetBehavior = new BottomSheetDialog(this);
        files_rv.setHasFixedSize(true);
        add_iv.setVisibility(View.VISIBLE);
        qr_iv.setVisibility(View.VISIBLE);
        initClick();
        file_bottom.getMenu().getItem(0).setTitle("Roster");
        files_rv.setLayoutManager(new LinearLayoutManager(Rosters.this));
        Glide.with(this).load(new Sessionmanager(Rosters.this).getToken().data.qrurl).into(qrcode_iv);

    }

    private void initClick() {
        file_bottom.setOnNavigationItemSelectedListener(this);
        add_iv.setOnClickListener(this);
        search_iv.setOnClickListener(this);
        delete_iv.setOnClickListener(this);
        profile_iv.setOnClickListener(this);
        qr_iv.setOnClickListener(this);

        search_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                filter(editable.toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(main_ln.getVisibility()==View.VISIBLE)
        super.onBackPressed();
        else{
            main_ln.setVisibility(View.VISIBLE);
            qrcode_iv.setVisibility(View.GONE);
        }

    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<RosterPojo.Datum> filterdNames = new ArrayList<>();

        //looping through existing elements
        for (int i = 0; i < list.size(); i++) {
            //if the existing elements contains the search input
            if (list.get(i).title!=null &&list.get(i).title.toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(list.get(i));
            }
        }
        if (isList)
            files_rv.setLayoutManager(new LinearLayoutManager(Rosters.this));
        else files_rv.setLayoutManager(new GridLayoutManager(Rosters.this, 3));
        files_rv.setAdapter(new RostersAdapter(Rosters.this, toolbar, filterdNames, all, recent, shared, trash));
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        hideSearch();
        add_iv.setVisibility(menuItem.getItemId() == R.id.files ? View.VISIBLE : View.GONE);
        if (menuItem.getItemId() == R.id.files) {
            changeTab(true, false, false, false);
            setData();
        } else if (menuItem.getItemId() == R.id.shared) {
            changeTab(false, false, true, false);
            setSharedData();
        } else if (menuItem.getItemId() == R.id.recent) {
            changeTab(false, true, false, false);
            setrecentData();
        } else if (menuItem.getItemId() == R.id.recycler) {
            changeTab(false, false, false, true);
            setTrashData();
        }
        return true;
    }

    private void changeTab(boolean b, boolean b1, boolean b2, boolean b3) {
        if (b)
            qr_iv.setVisibility(View.VISIBLE);
        else  qr_iv.setVisibility(View.GONE);
        all = b;
        recent = b1;
        shared = b2;
        trash = b3;
        if (!b3)
            delete_iv.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_iv) {
            v = getLayoutInflater().inflate(R.layout.noteadd_layout, null);
            sheetBehavior.setContentView(v);
            close = sheetBehavior.findViewById(R.id.close);
            option_ln = sheetBehavior.findViewById(R.id.option_ln);
            title_et = sheetBehavior.findViewById(R.id.title_et);
            note_et = sheetBehavior.findViewById(R.id.note_et);
            title_tv = sheetBehavior.findViewById(R.id.title_tv);
            create_rl = sheetBehavior.findViewById(R.id.create_rl);
            create_tv = sheetBehavior.findViewById(R.id.create_tv);
            title_tv.setText("Add Roster");
            close.setOnClickListener(this);
            create_tv.setOnClickListener(this);
            sheetBehavior.show();
        } else if (v.getId() == R.id.close)
            sheetBehavior.dismiss();
        else if (v.getId() == R.id.qr_iv)
            showQrCode();
        else if (v.getId() == R.id.profile_iv)
            startActivity(new Intent(Rosters.this, ApplicationPreferencesActivity.class));
        else if (v.getId() == R.id.delete_iv)
            deleteall();
        else if (v.getId() == R.id.create_tv) {
            if (title_et.getText().toString().trim().isEmpty()) {
                title_et.setError("Title is Required");
                title_et.requestFocus();
            }else if (note_et.getText().toString().trim().isEmpty()) {
                note_et.setError("Description is Required");
                note_et.requestFocus();
            } else
                createroster();
        } else if (v.getId() == R.id.search_iv) {
            CommonUtills.hideSoftKeyboard(Rosters.this);
            if (search_et.getVisibility() == View.GONE) {
                Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_from_top);
                search_et.setVisibility(View.VISIBLE);
                search_et.startAnimation(slideUp);
                search_iv.setImageResource(R.drawable.searchclose);
            } else
                hideSearch();
        }
    }

    private void showQrCode() {
        main_ln.setVisibility(main_ln.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        qrcode_iv.setVisibility(main_ln.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
    }

    private void hideSearch() {
        search_et.setVisibility(View.GONE);
        search_iv.setImageResource(R.drawable.search);
        search_et.setText("");
    }

    public void createroster() {
        CommonUtills.progresshow(Rosters.this);
        service.createRoster( title_et.getText().toString().trim(), note_et.getText().toString().trim()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().get("success").getAsBoolean()) {
                        sheetBehavior.dismiss();
                        setData();
                    }
                    CommonUtills.alertDialog(Rosters.this, "Roster created successfully");
                } else
                    CommonUtills.alertDialog(Rosters.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(Rosters.this, "Something went Wrong Please try again");
            }
        });
    }

    public void setData() {
        service.rosters().enqueue(new Callback<RosterPojo>() {
            @Override
            public void onResponse(Call<RosterPojo> call, Response<RosterPojo> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().status) {
                        if (response.body().data != null && response.body().data.size()>0) {
                            sheetBehavior.dismiss();
                            list = response.body().data;
                            files_rv.setVisibility(View.VISIBLE);
                            oops_tv.setVisibility(View.GONE);
                            files_rv.setAdapter(new RostersAdapter(Rosters.this, toolbar, response.body().data, all, recent, shared, trash));
                        } else {
                            oops_tv.setText("No Rosters found");
                            files_rv.setVisibility(View.GONE);
                            oops_tv.setVisibility(View.VISIBLE);
                        }
                    } else {
                        oops_tv.setText("No Rosters found");
                        files_rv.setVisibility(View.GONE);
                        oops_tv.setVisibility(View.VISIBLE);
                    }
                } else
                    CommonUtills.alertDialog(Rosters.this, "Something went Wrong Please try again");

                CommonUtills.progrescancel();
            }

            @Override
            public void onFailure(Call<RosterPojo> call, Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(Rosters.this, "Something went Wrong Please try again");
            }
        });
    }

    public void setSharedData() {
        CommonUtills.progresshow(Rosters.this);
        service.sharedrosters().enqueue(new Callback<RosterPojo>() {
            @Override
            public void onResponse(Call<RosterPojo> call, Response<RosterPojo> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().status) {
                        if (response.body().data != null && response.body().data.size()>0) {
                            sheetBehavior.dismiss();
                            list = response.body().data;
                            files_rv.setVisibility(View.VISIBLE);
                            oops_tv.setVisibility(View.GONE);
                            files_rv.setAdapter(new RostersAdapter(Rosters.this, toolbar, response.body().data, all, recent, shared, trash));
                        } else {
                            oops_tv.setText("No Rosters found");
                            files_rv.setVisibility(View.GONE);
                            oops_tv.setVisibility(View.VISIBLE);
                        }
                    } else {
                        oops_tv.setText("No Rosters found");
                        files_rv.setVisibility(View.GONE);
                        oops_tv.setVisibility(View.VISIBLE);
                    }
                } else
                    CommonUtills.alertDialog(Rosters.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(Call<RosterPojo> call, Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(Rosters.this, "Something went Wrong Please try again");
            }
        });
    }

    public void setrecentData() {
        CommonUtills.progresshow(Rosters.this);
        service.recentrosters().enqueue(new Callback<RosterPojo>() {
            @Override
            public void onResponse(Call<RosterPojo> call, Response<RosterPojo> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().status) {
                        if (response.body().data != null && response.body().data.size()>0) {
                            sheetBehavior.dismiss();
                            list = response.body().data;
                            files_rv.setVisibility(View.VISIBLE);
                            oops_tv.setVisibility(View.GONE);
                            files_rv.setAdapter(new RostersAdapter(Rosters.this, toolbar, response.body().data, all, recent, shared, trash));
                        } else {
                            oops_tv.setText("No Rosters found");
                            files_rv.setVisibility(View.GONE);
                            oops_tv.setVisibility(View.VISIBLE);
                        }
                    } else {
                        oops_tv.setText("No Rosters found");
                        files_rv.setVisibility(View.GONE);
                        oops_tv.setVisibility(View.VISIBLE);
                    }
                } else
                    CommonUtills.alertDialog(Rosters.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(Call<RosterPojo> call, Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(Rosters.this, "Something went Wrong Please try again");
            }
        });
    }

    public void setTrashData() {
        CommonUtills.progresshow(Rosters.this);
        service.trashrosters().enqueue(new Callback<RosterPojo>() {
            @Override
            public void onResponse(Call<RosterPojo> call, Response<RosterPojo> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().status) {
                        if (response.body().data != null && response.body().data.size()>0) {
                            sheetBehavior.dismiss();
                            list = response.body().data;
                            files_rv.setVisibility(View.VISIBLE);
                            oops_tv.setVisibility(View.GONE);
                            delete_iv.setVisibility(View.VISIBLE);
                            files_rv.setAdapter(new RostersAdapter(Rosters.this, toolbar, response.body().data, all, recent, shared, trash));
                        } else {
                            oops_tv.setText("No Rosters found");
                            files_rv.setVisibility(View.GONE);
                            oops_tv.setVisibility(View.VISIBLE);
                            delete_iv.setVisibility(View.GONE);
                        }
                    } else {
                        oops_tv.setText("No Rosters found");
                        files_rv.setVisibility(View.GONE);
                        oops_tv.setVisibility(View.VISIBLE);
                        delete_iv.setVisibility(View.GONE);
                    }
                } else
                    CommonUtills.alertDialog(Rosters.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(Call<RosterPojo> call, Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(Rosters.this, "Something went Wrong Please try again");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (new Sessionmanager(this).getToken().data.profileImg!=null)
            Glide.with(this).load(new Sessionmanager(this).getToken().data.profileImg).into(profile_iv);
        search_iv.setVisibility(View.VISIBLE);
        title_tv.setText("Roster");
        if (trash)
            setTrashData();
        else if (recent)
            setrecentData();
        else if (shared)
            setSharedData();
        else
            setData();

    }


    private void deleteall() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Rosters.this);
        builder.setMessage("Are you Sure?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                service.roster_all_delete(new Sessionmanager(Rosters.this).getToken().userId).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                        CommonUtills.progrescancel();
                        if (response.body() != null) {
                            if (response.body().get("status").getAsBoolean()) {
                                setTrashData();
                            }
                        } else
                            CommonUtills.alertDialog(Rosters.this, "Something went Wrong Please try again");
                    }

                    @Override
                    public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                        CommonUtills.progrescancel();
                        CommonUtills.alertDialog(Rosters.this, "Something went Wrong Please try again");
                    }
                });
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}