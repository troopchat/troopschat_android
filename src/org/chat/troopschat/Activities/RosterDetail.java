package org.chat.troopschat.Activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.chat.troopschat.Adapter.MembersAdapter;
import org.chat.troopschat.Pojo.Members;
import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;
import org.chat.troopschat.Utils.rest.ApiClient;
import org.chat.troopschat.Utils.rest.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RosterDetail extends AppCompatActivity implements View.OnClickListener {
    public ApiInterface service;
    ImageView back, add_iv, qr_iv, close, more_iv, save;
    TextView title_tv, oops_tv, membercount_tv, create_tv, created_tv, rostertitle_tv, desc_tv;
    EditText title_et, desc_et, email_et;
    LinearLayout main_ln, edit_ln, trash_ln;
    boolean isedit;
    RecyclerView items_rv;
    BottomSheetDialog sheetBehavior;
    LinearLayout toolbar, show_ln, scan_ln;
    boolean lock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sheetBehavior = new BottomSheetDialog(this);
        service = ApiClient.createService(ApiInterface.class, this);
        setContentView(R.layout.activity_roster_detail);
        initviews();
    }

    private void initviews() {
        back = findViewById(R.id.back);
        toolbar = findViewById(R.id.toolbar);
        membercount_tv = findViewById(R.id.membercount_tv);
        oops_tv = findViewById(R.id.oops_tv);
        more_iv = findViewById(R.id.more_iv);
        created_tv = findViewById(R.id.created_tv);
        rostertitle_tv = findViewById(R.id.rostertitle_tv);
        desc_tv = findViewById(R.id.desc_tv);
        qr_iv = findViewById(R.id.qr_iv);
        title_et = findViewById(R.id.title_et);
        desc_et = findViewById(R.id.desc_et);
        items_rv = findViewById(R.id.items_rv);
        save = findViewById(R.id.save);
        title_tv = findViewById(R.id.title_tv);
        main_ln = findViewById(R.id.main_ln);
        title_et = findViewById(R.id.title_et);
        add_iv = findViewById(R.id.add_iv);
        lock = getIntent().getBooleanExtra("lock", false);
        items_rv.setLayoutManager(new LinearLayoutManager(RosterDetail.this));
        initData();
        getMembers();
        initClick();
    }

    public void getMembers() {
        CommonUtills.progresshow(RosterDetail.this);
        service.rostermember(getIntent().getStringExtra("id")).enqueue(new Callback<Members>() {
            @Override
            public void onResponse(@NonNull Call<Members> call, @NonNull Response<Members> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().success) {
                        created_tv.setText(response.body().data.user_name);
                        membercount_tv.setText("Members (" + response.body().data.getMember.size() + ")");
                        items_rv.setAdapter(new MembersAdapter(RosterDetail.this, getIntent().getStringExtra("id"), response.body().data.getMember, toolbar));
                        items_rv.setVisibility(View.VISIBLE);
                        oops_tv.setVisibility(View.GONE);
                    } else {
                        membercount_tv.setText("Members (" + 0 + ")");
                        items_rv.setVisibility(View.GONE);
                        oops_tv.setVisibility(View.VISIBLE);
                    }
                } else
                    CommonUtills.alertDialog(RosterDetail.this, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(@NonNull Call<Members> call, @NonNull Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(RosterDetail.this, "Something went Wrong Please try again");
            }
        });
    }

    private void initClick() {
        back.setOnClickListener(this);
        save.setOnClickListener(this);
        more_iv.setOnClickListener(this);
        add_iv.setOnClickListener(this);
        qr_iv.setOnClickListener(this);
    }

    private void initData() {
        title_tv.setText(getIntent().getStringExtra("title"));
        title_et.setText(getIntent().getStringExtra("title"));

        rostertitle_tv.setText(getIntent().getStringExtra("title"));
        desc_tv.setText(getIntent().getStringExtra("desc"));
        desc_et.setText(getIntent().getStringExtra("desc"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.add_iv:
                v = getLayoutInflater().inflate(R.layout.addmemberlayout, null);
                sheetBehavior.setContentView(v);
                email_et = sheetBehavior.findViewById(R.id.email_et);
                create_tv = sheetBehavior.findViewById(R.id.add_tv);
                create_tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (email_et.getText().toString().trim().isEmpty()) {
                            email_et.requestFocus();
                            email_et.setError("Member Email is required");
                        } else
                            addMember(email_et.getText().toString().trim());
                    }
                });
                sheetBehavior.show();
                break;
            case R.id.qr_iv:
                checkPermission();

                break;

            case R.id.more_iv:
                v = getLayoutInflater().inflate(R.layout.rosterdetailmore_layout, null);
                sheetBehavior.setContentView(v);
                close = sheetBehavior.findViewById(R.id.close);
                edit_ln = sheetBehavior.findViewById(R.id.edit_ln);
                trash_ln = sheetBehavior.findViewById(R.id.trash_ln);
                edit_ln.setVisibility(isedit ? View.GONE : View.VISIBLE);
                trash_ln.setOnClickListener(this);
                edit_ln.setOnClickListener(this);
                sheetBehavior.show();
                break;
            case R.id.save:
                upDate();
                break;
            case R.id.edit_ln:
                isedit = true;
                initEdit();
                sheetBehavior.dismiss();
                break;
            case R.id.trash_ln:
                delete();
                break;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(RosterDetail.this, Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(RosterDetail.this, Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat.checkSelfPermission(RosterDetail.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(RosterDetail.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(RosterDetail.this, Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(RosterDetail.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Snackbar.make(RosterDetail.this.findViewById(android.R.id.content), "Please Grant Permissions to upload profile photo", Snackbar.LENGTH_INDEFINITE).setAction("ENABLE", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 012);
                    }
                }).show();
            } else {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 012);
            }
        } else {
            scanQrCode();
        }
    }

    private void scanQrCode() {
        startActivityForResult(new Intent(this, QrCodeScanner.class), 101);
    }

    private void addMember(String member) {
        CommonUtills.progresshow(this);
        service.createRostermember(getIntent().getStringExtra("id"), member).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().get("success").getAsBoolean()) {
                        getMembers();
                        sheetBehavior.dismiss();
                        CommonUtills.alertDialog(RosterDetail.this, "Member added successfully");
                    }

                 else CommonUtills.alertDialog(RosterDetail.this, response.body().get("message").getAsString());
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(RosterDetail.this, "Something went Wrong Please try again");
            }
        });
    }

    private void delete() {
        final ArrayList<String> id = new ArrayList<>();
        id.add(getIntent().getStringExtra("id"));
        AlertDialog.Builder builder = new AlertDialog.Builder(RosterDetail.this);
        builder.setMessage("Are you Sure?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                service.trashRoster(id).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                        CommonUtills.progrescancel();
                        if (response.body() != null) {
                            if (response.body().get("success").getAsBoolean()) {
                                sheetBehavior.dismiss();
                                finish();
                            } else
                                CommonUtills.alertDialog(RosterDetail.this, response.body().get("message").getAsString());

                        } else
                            CommonUtills.alertDialog(RosterDetail.this, "Something went Wrong Please try again");
                    }

                    @Override
                    public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                        CommonUtills.progrescancel();
                        CommonUtills.alertDialog(RosterDetail.this, "Something went Wrong Please try again");
                    }
                });
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void initEdit() {
        title_et.setVisibility(isedit ? View.VISIBLE : View.GONE);
        rostertitle_tv.setVisibility(isedit ? View.GONE : View.VISIBLE);
        desc_et.setVisibility(isedit ? View.VISIBLE : View.GONE);
        desc_tv.setVisibility(isedit ? View.GONE : View.VISIBLE);
        back.setVisibility(isedit ? View.GONE : View.VISIBLE);
        save.setVisibility(isedit ? View.VISIBLE : View.GONE);
        title_tv.setText(title_et.getText().toString());
        rostertitle_tv.setText(title_et.getText().toString());
        desc_tv.setText(desc_et.getText().toString());
    }

    public void upDate() {
        if (title_et.getText().toString().trim().isEmpty()) {
            title_et.setError("Title is Required");
            title_et.requestFocus();
        } else {
            CommonUtills.progresshow(RosterDetail.this);
            service.editRoster(getIntent().getStringExtra("id"), title_et.getText().toString().trim(), desc_et.getText().toString().trim()).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                    CommonUtills.progrescancel();
                    if (response.body() != null) {
                        if (response.body().get("success").getAsBoolean()) {
                            isedit = false;
                            initEdit();
                            sheetBehavior.dismiss();
                        } else
                            CommonUtills.alertDialog(RosterDetail.this, response.body().get("message").getAsString());
                    } else
                        CommonUtills.alertDialog(RosterDetail.this, "Something went Wrong Please try again");
                }

                @Override
                public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                    CommonUtills.progrescancel();
                    CommonUtills.alertDialog(RosterDetail.this, "Something went Wrong Please try again");
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("Contents");
                addMember(result);
            }
        }
    }
}