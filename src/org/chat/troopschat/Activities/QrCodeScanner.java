package org.chat.troopschat.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.zxing.Result;

import org.chat.troopschat.R;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QrCodeScanner extends AppCompatActivity implements  ZXingScannerView.ResultHandler {
    ZXingScannerView zXingScannerView;
    boolean status = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        scanQrCode();
    }

    private void scanQrCode() {
        zXingScannerView = new ZXingScannerView(QrCodeScanner.this);
        setContentView(zXingScannerView);
        zXingScannerView.setResultHandler(QrCodeScanner.this);
        status = true;
        zXingScannerView.startCamera();
    }

    @Override
    public void handleResult(Result result) {
        Intent intent=new Intent();
        intent.putExtra("Contents",result.getText());
        setResult(RESULT_OK,intent);
        finish();
    }
}
