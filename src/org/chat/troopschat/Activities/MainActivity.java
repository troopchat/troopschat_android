package org.chat.troopschat.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.chat.troopschat.ApplicationPreferencesActivity;
import org.chat.troopschat.ConversationListActivity;
import org.chat.troopschat.R;
import org.chat.troopschat.Utils.Sessionmanager;
import org.chat.troopschat.Utils.rest.ApiClient;
import org.chat.troopschat.Utils.rest.ApiInterface;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static ApiInterface service;
    public static Sessionmanager sessionmanager;
    ImageView add_iv, profile_iv, search_iv;
    TextView title_tv;
    CardView chat_cv, files_cv, note_cv, roster_cv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sessionmanager = new Sessionmanager(this);
        service = ApiClient.createService(ApiInterface.class, MainActivity.this);
        add_iv = findViewById(R.id.add_iv);
        profile_iv = findViewById(R.id.profile_iv);
        search_iv = findViewById(R.id.search_iv);
        title_tv = findViewById(R.id.title_tv);
        chat_cv = findViewById(R.id.chat_cv);
        files_cv = findViewById(R.id.files_cv);
        note_cv = findViewById(R.id.note_cv);
        roster_cv = findViewById(R.id.roster_cv);
        chat_cv.setOnClickListener(this);
        files_cv.setOnClickListener(this);
        note_cv.setOnClickListener(this);
        roster_cv.setOnClickListener(this);
        profile_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ApplicationPreferencesActivity.class));
            }
        });

    }

    @Override
    protected void onResume() {
        add_iv.setVisibility(View.GONE);
        search_iv.setVisibility(View.GONE);
        title_tv.setText("Home");
        if (sessionmanager.getToken().data.profileImg!=null)
            com.bumptech.glide.Glide.with(this).load(sessionmanager.getToken().data.profileImg).into(profile_iv);
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        Class name = null;
        if (v.getId() == R.id.chat_cv) {
            name = ConversationListActivity.class;
        } else if (v.getId() == R.id.files_cv)
            name = Files.class;
        else if (v.getId() == R.id.note_cv)
            name = Notes.class;
        else if (v.getId() == R.id.roster_cv)
            name = Rosters.class;

        try {
            startActivity(new Intent(MainActivity.this, name));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
