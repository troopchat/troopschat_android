package org.chat.troopschat.Activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.chat.troopschat.ConversationListActivity;
import org.chat.troopschat.Pojo.User;
import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;
import org.chat.troopschat.Utils.Sessionmanager;
import org.chat.troopschat.Utils.rest.ApiClient;
import org.chat.troopschat.Utils.rest.ApiInterface;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.chat.troopschat.Utils.Constants.Pick_gallery;

public class Settings extends AppCompatActivity implements View.OnClickListener {
    LinearLayout profile_ll, settings_ll, profileupdate_ll, logout_ll, roster_ll, file_ll, notes_ll, chat_ll;
    TextView name_tv, title_tv, finish_tv;
    ImageView profile_iv, back, profileupdate_iv;
    FrameLayout profile_fl;
    EditText name_et;
    Sessionmanager sessionmanager;
    Bitmap bm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        sessionmanager = new Sessionmanager(this);
        initViews();
    }

    private void initViews() {
        profile_ll = findViewById(R.id.profile_ll);
        roster_ll = findViewById(R.id.roster_ll);
        name_et = findViewById(R.id.name_et);
        settings_ll = findViewById(R.id.settings_ll);
        profileupdate_ll = findViewById(R.id.profileupdate_ll);
        file_ll = findViewById(R.id.file_ll);
        profileupdate_iv = findViewById(R.id.profileupdate_iv);
        profile_fl = findViewById(R.id.profile_fl);
        finish_tv = findViewById(R.id.finish_tv);
        logout_ll = findViewById(R.id.logout_ll);
        back = findViewById(R.id.back);
        notes_ll = findViewById(R.id.notes_ll);
        title_tv = findViewById(R.id.title_tv);
        chat_ll = findViewById(R.id.chat_ll);
        name_tv = findViewById(R.id.name_tv);
        profile_iv = findViewById(R.id.profile_iv);
        initdata();
        initclick();
    }

    private void initclick() {
        logout_ll.setOnClickListener(this);
        profile_ll.setOnClickListener(this);
        profile_iv.setOnClickListener(this);
        profile_fl.setOnClickListener(this);
        profileupdate_iv.setOnClickListener(this);
        roster_ll.setOnClickListener(this);
        file_ll.setOnClickListener(this);
        notes_ll.setOnClickListener(this);
        chat_ll.setOnClickListener(this);
        finish_tv.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    private void initdata() {
        name_tv.setText(sessionmanager.getToken().data.fullName);
        name_et.setText(sessionmanager.getToken().data.fullName);
        if (!sessionmanager.getToken().data.profileImg.isEmpty()) {
            Glide.with(this).load(sessionmanager.getToken().data.profileImg).into(profile_iv);
            Glide.with(this).load(sessionmanager.getToken().data.profileImg).into(profileupdate_iv);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.logout_ll:
                startActivity(new Intent(Settings.this, LoginActivity.class));
                sessionmanager.clearSharedPreference();
                finishAffinity();
                break;
            case R.id.roster_ll:
                startActivity(new Intent(Settings.this, Rosters.class));
                break;
            case R.id.file_ll:
                startActivity(new Intent(Settings.this, Files.class));
                break;
            case R.id.notes_ll:
                startActivity(new Intent(Settings.this, Notes.class));
                break;
            case R.id.chat_ll:
               startActivity(new Intent(Settings.this, ConversationListActivity.class));
                break;
            case R.id.profile_ll:
                changeSettings();
                break;
            case R.id.profile_iv:
                changeSettings();
                break;
            case R.id.profileupdate_iv:
                checkPermission();
                break;
            case R.id.back:
                onBackPressed();
                break;
            case R.id.finish_tv:
                if (name_et.getText().toString().isEmpty()) {
                    name_et.setError(getString(R.string.nameerror));
                    name_et.requestFocus();
                } else
                    updateProfile();
                break;
        }
    }

    private void changeSettings() {
        profileupdate_ll.setVisibility(settings_ll.getVisibility() == View.GONE ? View.GONE : View.VISIBLE);
        finish_tv.setVisibility(settings_ll.getVisibility() == View.GONE ? View.GONE : View.VISIBLE);
        settings_ll.setVisibility(settings_ll.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
        title_tv.setText(settings_ll.getVisibility() == View.VISIBLE ? "Settings" : "Update Profile");
    }

    @Override
    public void onBackPressed() {
        if (settings_ll.getVisibility() == View.VISIBLE)
            super.onBackPressed();
        else changeSettings();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(Settings.this, Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(Settings.this, Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat.checkSelfPermission(Settings.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(Settings.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(Settings.this, Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(Settings.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Snackbar.make(Settings.this.findViewById(android.R.id.content), "Please Grant Permissions to upload profile photo", Snackbar.LENGTH_INDEFINITE).setAction("ENABLE", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 012);
                    }
                }).show();
            } else {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 012);
            }
        } else {
            selectImage();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            Toast.makeText(getApplicationContext(), "Try Downloading Now", Toast.LENGTH_LONG).show();
        } else {
            boolean cam_Permission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
            boolean write_Permission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
            boolean storage_Permission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
            if (write_Permission && storage_Permission && cam_Permission) {
                selectImage();
            }
        }
    }

    private void selectImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Photo"), Pick_gallery);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Pick_gallery) {
                try {
                    bm = MediaStore.Images.Media.getBitmap(Settings.this.getContentResolver(), data.getData());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (bm != null)
                profileupdate_iv.setImageBitmap(bm);
        }
    }

    private void updateProfile() {
        ApiInterface service = ApiClient.createService(ApiInterface.class, Settings.this);
        CommonUtills.progresshow(Settings.this);
        MultipartBody.Part fileToUpload = null;
        if (bm != null) {
            File file = new File(CommonUtills.getRealPathFromURI(Settings.this, CommonUtills.getImageUri(Settings.this, bm)));
            RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            fileToUpload = MultipartBody.Part.createFormData("profile_pic", file.getName(), mFile);
        }
        RequestBody login_token = RequestBody.create(MediaType.parse("text/plain"), new Sessionmanager(Settings.this).getToken().userId);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), name_et.getText().toString().trim());
        service.update_profile(fileToUpload, login_token, name).enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
//                CommonUtills.progrescancel();
//                CommonUtills.alertDialog(Settings.this, response.body().message);
//                sessionmanager.setLogin(true, response.body());
//                changeSettings();
//                initdata();
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(Settings.this, "Something went Wrong Please try again");
            }
        });
    }
}