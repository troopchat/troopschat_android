/*
 * Copyright (C) 2011 Whisper Systems
 * Copyright (C) 2013-2017 Open Whisper Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.chat.troopschat;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.preference.Preference;

import com.google.gson.JsonObject;

import org.chat.troopschat.Activities.Files;
import org.chat.troopschat.Activities.LoginActivity;
import org.chat.troopschat.Activities.Notes;
import org.chat.troopschat.Activities.Rosters;
import org.chat.troopschat.Utils.CommonUtills;
import org.chat.troopschat.Utils.Sessionmanager;
import org.chat.troopschat.Utils.rest.ApiClient;
import org.chat.troopschat.Utils.rest.ApiInterface;
import org.chat.troopschat.preferences.AppProtectionPreferenceFragment;
import org.chat.troopschat.preferences.ChatsPreferenceFragment;
import org.chat.troopschat.preferences.CorrectedPreferenceFragment;
import org.chat.troopschat.preferences.NotificationsPreferenceFragment;
import org.chat.troopschat.preferences.widgets.ProfilePreference;
import org.chat.troopschat.service.KeyCachingService;
import org.chat.troopschat.util.DynamicLanguage;
import org.chat.troopschat.util.DynamicTheme;
import org.chat.troopschat.util.TextSecurePreferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * The Activity for application preference display and management.
 *
 * @author Moxie Marlinspike
 */

public class ApplicationPreferencesActivity extends PassphraseRequiredActionBarActivity
        implements SharedPreferences.OnSharedPreferenceChangeListener {
  @SuppressWarnings("unused")
  private static final String TAG = ApplicationPreferencesActivity.class.getSimpleName();

  private static final String PREFERENCE_CATEGORY_PROFILE = "preference_category_profile";
  private static final String PREFERENCE_CATEGORY_NOTIFICATIONS = "preference_category_notifications";
  private static final String PREFERENCE_CATEGORY_APP_PROTECTION = "preference_category_app_protection";
  private static final String PREFERENCE_CATEGORY_CHATS = "preference_category_chats";
  private static final String PREFERENCE_CATEGORY_DEVICES = "preference_category_devices";
  private static final String files = "files";
  private static final String notes = "notes";
  private static final String storage = "storage";
  private static final String rosters = "rosters";
  private static final String logout = "logout";

  private final DynamicTheme dynamicTheme = new DynamicTheme();
  private final DynamicLanguage dynamicLanguage = new DynamicLanguage();

  @Override
  protected void onPreCreate() {
    dynamicTheme.onCreate(this);
    dynamicLanguage.onCreate(this);
  }

  @Override
  protected void onCreate(Bundle icicle, boolean ready) {
    //noinspection ConstantConditions
    this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    if (getIntent() != null && getIntent().getCategories() != null && getIntent().getCategories().contains("android.intent.category.NOTIFICATION_PREFERENCES")) {
      initFragment(android.R.id.content, new NotificationsPreferenceFragment());
    } else if (icicle == null) {
      initFragment(android.R.id.content, new ApplicationPreferenceFragment());
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    dynamicTheme.onResume(this);
    dynamicLanguage.onResume(this);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    Fragment fragment = getSupportFragmentManager().findFragmentById(android.R.id.content);
    fragment.onActivityResult(requestCode, resultCode, data);
  }

  @Override
  public boolean onSupportNavigateUp() {
    FragmentManager fragmentManager = getSupportFragmentManager();
    if (fragmentManager.getBackStackEntryCount() > 0) {
      fragmentManager.popBackStack();
    } else {
      Intent intent = new Intent(this, ConversationListActivity.class);
      intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      startActivity(intent);
      finish();
    }
    return true;
  }

  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    if (key.equals(TextSecurePreferences.THEME_PREF)) {
      recreate();
    } else if (key.equals(TextSecurePreferences.LANGUAGE_PREF)) {
      recreate();

      Intent intent = new Intent(this, KeyCachingService.class);
      intent.setAction(KeyCachingService.LOCALE_CHANGE_EVENT);
      startService(intent);
    }
  }

  public static class ApplicationPreferenceFragment extends CorrectedPreferenceFragment {

    @Override
    public void onCreate(Bundle icicle) {
      super.onCreate(icicle);
      setData();
      this.findPreference(PREFERENCE_CATEGORY_PROFILE)
              .setOnPreferenceClickListener(new ProfileClickListener());
      this.findPreference(files)
              .setOnPreferenceClickListener(new FilesClickListener());
      this.findPreference(notes)
              .setOnPreferenceClickListener(new NotesClickListener());
      this.findPreference(rosters)
              .setOnPreferenceClickListener(new RostersClickListener());
      this.findPreference(logout)
              .setOnPreferenceClickListener(new LogoutClickListener());
      this.findPreference(PREFERENCE_CATEGORY_NOTIFICATIONS)
              .setOnPreferenceClickListener(new CategoryClickListener(PREFERENCE_CATEGORY_NOTIFICATIONS));
      this.findPreference(PREFERENCE_CATEGORY_APP_PROTECTION)
              .setOnPreferenceClickListener(new CategoryClickListener(PREFERENCE_CATEGORY_APP_PROTECTION));
      this.findPreference(PREFERENCE_CATEGORY_CHATS)
              .setOnPreferenceClickListener(new CategoryClickListener(PREFERENCE_CATEGORY_CHATS));
      this.findPreference(PREFERENCE_CATEGORY_DEVICES)
              .setOnPreferenceClickListener(new CategoryClickListener(PREFERENCE_CATEGORY_DEVICES));

      if (VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
        tintIcons(getActivity());
      }
    }

    @Override
    public void onCreatePreferences(@Nullable Bundle savedInstanceState, String rootKey) {
      addPreferencesFromResource(R.xml.preferences);
    }

    public void setData() {
      ApiInterface service = ApiClient.createService(ApiInterface.class, getActivity());
      CommonUtills.progresshow(getActivity());
      service.getStorageqouta(new Sessionmanager(getActivity()).getToken().userId).enqueue(new Callback<JsonObject>() {
        @Override
        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
          CommonUtills.progrescancel();
          if (response.body() != null) {
            if (response.body().get("status").getAsBoolean()) {
              ApplicationPreferenceFragment.this.findPreference(storage)
                      .setTitle(String.format("Storage Used: %s", response.body().get("data").getAsString()));
            } else
              CommonUtills.alertDialog(getActivity(), "Something went Wrong Please try again");
          }
        }

        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
          CommonUtills.progrescancel();
          CommonUtills.alertDialog(getActivity(), "Something went Wrong Please try again");
        }
      });
    }

    @Override
    public void onResume() {
      super.onResume();
      ((ApplicationPreferencesActivity) getActivity()).getSupportActionBar().setTitle(R.string.text_secure_normal__menu_settings);
      setCategorySummaries();
      setCategoryVisibility();
    }

    private void setCategorySummaries() {
      ((ProfilePreference) this.findPreference(PREFERENCE_CATEGORY_PROFILE)).refresh();
      this.findPreference(PREFERENCE_CATEGORY_NOTIFICATIONS)
              .setSummary(NotificationsPreferenceFragment.getSummary(getActivity()));
      this.findPreference(PREFERENCE_CATEGORY_APP_PROTECTION)
              .setSummary(AppProtectionPreferenceFragment.getSummary(getActivity()));
      this.findPreference(PREFERENCE_CATEGORY_CHATS)
              .setSummary(ChatsPreferenceFragment.getSummary(getActivity()));
    }

    private void setCategoryVisibility() {
      Preference devicePreference = this.findPreference(PREFERENCE_CATEGORY_DEVICES);
      if (devicePreference != null && !TextSecurePreferences.isPushRegistered(getActivity())) {
        getPreferenceScreen().removePreference(devicePreference);
      }
    }

    @TargetApi(11)
    private void tintIcons(Context context) {
      Drawable sms = DrawableCompat.wrap(ContextCompat.getDrawable(context, R.drawable.ic_textsms_white_24dp));
      Drawable notifications = DrawableCompat.wrap(ContextCompat.getDrawable(context, R.drawable.ic_notifications_white_24dp));
      Drawable privacy = DrawableCompat.wrap(ContextCompat.getDrawable(context, R.drawable.ic_security_white_24dp));
      Drawable chats = DrawableCompat.wrap(ContextCompat.getDrawable(context, R.drawable.ic_forum_white_24dp));
      Drawable devices = DrawableCompat.wrap(ContextCompat.getDrawable(context, R.drawable.ic_laptop_white_24dp));

      int[] tintAttr = new int[]{R.attr.pref_icon_tint};
      TypedArray typedArray = context.obtainStyledAttributes(tintAttr);
      int color = typedArray.getColor(0, 0x0);
      typedArray.recycle();

      DrawableCompat.setTint(sms, color);
      DrawableCompat.setTint(notifications, color);
      DrawableCompat.setTint(privacy, color);
      DrawableCompat.setTint(chats, color);
      DrawableCompat.setTint(devices, color);

      this.findPreference(PREFERENCE_CATEGORY_NOTIFICATIONS).setIcon(notifications);
      this.findPreference(PREFERENCE_CATEGORY_APP_PROTECTION).setIcon(privacy);
      this.findPreference(PREFERENCE_CATEGORY_CHATS).setIcon(chats);
      this.findPreference(PREFERENCE_CATEGORY_DEVICES).setIcon(devices);
    }

    private class CategoryClickListener implements Preference.OnPreferenceClickListener {
      private String category;

      CategoryClickListener(String category) {
        this.category = category;
      }

      @Override
      public boolean onPreferenceClick(Preference preference) {
        Fragment fragment = null;

        switch (category) {
          case PREFERENCE_CATEGORY_NOTIFICATIONS:
            fragment = new NotificationsPreferenceFragment();
            break;
          case PREFERENCE_CATEGORY_APP_PROTECTION:
            fragment = new AppProtectionPreferenceFragment();
            break;
          case PREFERENCE_CATEGORY_CHATS:
            fragment = new ChatsPreferenceFragment();
            break;
          case PREFERENCE_CATEGORY_DEVICES:
            Intent intent = new Intent(getActivity(), DeviceActivity.class);
            startActivity(intent);
            break;
          default:
            throw new AssertionError();
        }

        if (fragment != null) {
          Bundle args = new Bundle();
          fragment.setArguments(args);

          FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
          FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
          fragmentTransaction.replace(android.R.id.content, fragment);
          fragmentTransaction.addToBackStack(null);
          fragmentTransaction.commit();
        }

        return true;
      }
    }

    private class ProfileClickListener implements Preference.OnPreferenceClickListener {
      @Override
      public boolean onPreferenceClick(Preference preference) {
        Intent intent = new Intent(preference.getContext(), CreateProfileActivity.class);
        intent.putExtra(CreateProfileActivity.EXCLUDE_SYSTEM, true);

        getActivity().startActivity(intent);
        return true;
      }
    }

    private class RostersClickListener implements Preference.OnPreferenceClickListener {
      @Override
      public boolean onPreferenceClick(Preference preference) {
        Intent intent = new Intent(preference.getContext(), Rosters.class);
        getActivity().startActivity(intent);
        return true;
      }
    }

    private class LogoutClickListener implements Preference.OnPreferenceClickListener {
      @Override
      public boolean onPreferenceClick(Preference preference) {
        startActivity(new Intent(getActivity(), LoginActivity.class));
        new Sessionmanager(getActivity()).clearSharedPreference();
        getActivity().finishAffinity();
        return true;
      }
    }

    private class NotesClickListener implements Preference.OnPreferenceClickListener {
      @Override
      public boolean onPreferenceClick(Preference preference) {
        Intent intent = new Intent(preference.getContext(), Notes.class);
        getActivity().startActivity(intent);
        return true;
      }
    }

    private class FilesClickListener implements Preference.OnPreferenceClickListener {
      @Override
      public boolean onPreferenceClick(Preference preference) {
        Intent intent = new Intent(preference.getContext(), Files.class);
        getActivity().startActivity(intent);
        return true;
      }
    }
  }
}