package org.chat.troopschat;

public interface Unbindable {
  public void unbind();
}
