package org.chat.troopschat;

public interface MasterSecretListener {
  void onMasterSecretCleared();
}
