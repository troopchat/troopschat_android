package org.chat.troopschat.Utils.rest;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    public static String BASE_URL = "http://165.227.92.144/api/";
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static Retrofit retrofit;

    public static <S> S createService(Class<S> serviceClass, Activity context) {
        return createService(serviceClass, context, null);
    }

    public static <S> S createService(Class<S> serviceClass, final Activity context, final String authToken) {
        try {
            if (authToken != null) {
                httpClient.addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(@NonNull Interceptor.Chain chain) throws IOException {
                        Request original = chain.request();
                        Request.Builder requestBuilder = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .header("Authorization", "Bearer " + authToken)
                                .method(original.method(), original.body());
                        if (requestBuilder != null) {
                            Request request = requestBuilder.build();
                            return chain.proceed(request);
                        }
                        return null;
                    }
                });
            }
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = httpClient.addInterceptor(interceptor).
                    connectTimeout(1, TimeUnit.HOURS)
                    .readTimeout(1, TimeUnit.HOURS).build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(new Gson()))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create()).client(client).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retrofit.create(serviceClass);
    }
}