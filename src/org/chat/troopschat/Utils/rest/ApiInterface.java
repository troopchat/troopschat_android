package org.chat.troopschat.Utils.rest;

import com.google.gson.JsonObject;

import org.chat.troopschat.Pojo.CheckListItemPojo;
import org.chat.troopschat.Pojo.CheckListPojo;
import org.chat.troopschat.Pojo.Members;
import org.chat.troopschat.Pojo.NotesPojo;
import org.chat.troopschat.Pojo.RosterPojo;
import org.chat.troopschat.Pojo.User;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @POST("register")
    Call<JsonObject> signup(@Body JsonObject jsonObject);

    @POST("login")
    Call<User> login(@Body JsonObject jsonObject);

    @FormUrlEncoded
    @POST("folders")
    Call<JsonObject> createFolder(@Field("title") String email);

    @Multipart
    @POST("files")
    Call<JsonObject> uploadFile(@Part MultipartBody.Part file, @Part("folder_id") RequestBody folder_id);

    @Multipart
    @POST("update_profile")
    Call<User> update_profile(@Part MultipartBody.Part file, @Part("user_id") RequestBody id, @Part("user_name") RequestBody user_name);

    @FormUrlEncoded
    @POST("forgot_password")
    Call<JsonObject> forget(@Field("user_email") String email);

    @FormUrlEncoded
    @POST("roster_all_delete")
    Call<JsonObject> roster_all_delete(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("file-rename")
    Call<JsonObject> rename(@Field("file_name") String new_name,@Field("type") String type, @Field("file_id") String id);
    @FormUrlEncoded
    @POST("file-delete-multiple")
    Call<JsonObject> deletefile(@Field("type") String type, @Field("file_id[]") ArrayList<String> id);

    @FormUrlEncoded
    @POST("file-share-delete ")
    Call<JsonObject> deletesharedfile(@Field("type") String type, @Field("id[]") ArrayList<String> id);

    @FormUrlEncoded
    @POST("file-share-multiple")
    Call<JsonObject> shareemail( @Field("email") String shared_user_email, @Field("type") String type, @Field("file_id[]") ArrayList<String> id);

    @FormUrlEncoded
    @POST("files-move")
    Call<JsonObject> moveFile(@Field("file_id[]") ArrayList<String> file_id, @Field("folder_id") String folder_id);

    @FormUrlEncoded
    @POST("notes-delete")
    Call<JsonObject> deletenote(@Field("note_id[]") ArrayList<String> note_id);

    @FormUrlEncoded
    @POST("rosters-delete")
    Call<JsonObject> deleteRoster(@Field("id[]") ArrayList<String> note_id);

    @FormUrlEncoded
    @POST("notes-trash")
    Call<JsonObject> trashnote(@Field("note_id[]") ArrayList<String> note_id);

    @FormUrlEncoded
    @POST("rosters-trash")
    Call<JsonObject> trashRoster(@Field("roster_id[]") ArrayList<String> note_id);

    @FormUrlEncoded
    @POST("notes-restore")
    Call<JsonObject> restoretrash(@Field("note_id[]") ArrayList<String> note_id);

    @FormUrlEncoded
    @POST("rosters-restore")
    Call<JsonObject> restorerostertrash(@Field("id[]") ArrayList<String> note_id);

    @FormUrlEncoded
    @POST("checklists-restore")
    Call<JsonObject> restorechecktrash(@Field("checklist_id[]") ArrayList<String> checklist_id);

    @FormUrlEncoded
    @POST("checklists-delete")
    Call<JsonObject> deletechecklist(@Field("checklist_id[]") ArrayList<String> checklist_id);

    @FormUrlEncoded
    @POST("checklists-trash")
    Call<JsonObject> trashchecklist(@Field("checklist_id[]") ArrayList<String> checklist_id);

    @FormUrlEncoded
    @POST("items-delete")
    Call<JsonObject> delechecklistItem(@Field("item_id[]") ArrayList<String> item_id);

    @FormUrlEncoded
    @POST("rosters-delete-member")
    Call<JsonObject> deleterostermember(@Field("id[]") ArrayList<String> member_email);

    @FormUrlEncoded
    @POST("items")
    Call<JsonObject> ChecklistItem(@Field("checklist_id") String checklist_id, @Field("title") String item_title,@Field("status") int status);


    @FormUrlEncoded
    @POST("items-update")
    Call<JsonObject> updatelistItem(@Field("id") String checklist_id, @Field("title") String item_title, @Field("status") int item_status);

    @FormUrlEncoded
    @POST("notes")
    Call<JsonObject> createNote( @Field("title") String title, @Field("description") String discription, @Field("color") String color, @Field("checked") int status, @Field("lock") int lock);

    @FormUrlEncoded
    @POST("rosters")
    Call<JsonObject> createRoster( @Field("title") String title, @Field("description") String discription);

    @FormUrlEncoded
    @POST("rosters-add-member")
    Call<JsonObject> createRostermember(@Field("roster_id") String roster_id, @Field("email") String member_name);

    @FormUrlEncoded
    @POST("rosters-update")
    Call<JsonObject> editRoster(@Field("id") String roster_id, @Field("title") String title, @Field("description") String discription);

    @FormUrlEncoded
    @POST("checklists")
    Call<JsonObject> createChecklist(@Field("title") String check_list_title,
                                     @Field("color") String color, @Field("lock") int lock,@Field("checked") int check);

    @FormUrlEncoded
    @POST("checklists-update")
    Call<JsonObject> updateChecklist(@Field("id") String checklist_id, @Field("title") String check_list_title,
                                     @Field("color") String color, @Field("lock") int lock,@Field("checked") int check);

    @FormUrlEncoded
    @POST("notes-update")
    Call<JsonObject> updateNote(@Field("id") String note_id, @Field("title") String title, @Field("description") String discription, @Field("color") String color, @Field("checked") int status, @Field("lock") int lock);

    @GET("folders")
    Call<JsonObject> files();

    @GET("folders-shared-list")
    Call<JsonObject> sharedFiles();

    @GET("notes")
    Call<NotesPojo> notes();

    @GET("user_space_quota")
    Call<JsonObject> getStorageqouta(@Query("user_id") String id);

    @FormUrlEncoded
    @POST("notes-trash-all-delete")
    Call<JsonObject> emptynotestrash();

    @FormUrlEncoded
    @POST("checklists-trash-all-delete")
    Call<JsonObject> emptychecktrash();

    @GET("rosters")
    Call<RosterPojo> rosters();

    @GET("rosters-shared-list")
    Call<RosterPojo> sharedrosters();

    @GET("rosters-recents")
    Call<RosterPojo> recentrosters();

    @GET("rosters-trash-list")
    Call<RosterPojo> trashrosters();

    @GET("notes-trash-list")
    Call<NotesPojo> trashnotes();

    @GET("checklists-trash-list")
    Call<CheckListPojo> trashchecklist();

    @GET("checklists")
    Call<CheckListPojo> checklist();

    @GET("folders-data/{folder_id}")
    Call<JsonObject> folderdata(@Path("folder_id") String id);

    @GET("items")
    Call<CheckListItemPojo> Checklistitems(@Query("checklist_id") String id);

    @GET("rosters/{roster_id}")
    Call<Members> rostermember(@Path("roster_id") String id);

    @GET("folders-recents")
    Call<JsonObject> recenFiles();

    @FormUrlEncoded
    @POST("notes-color-update")
    Call<JsonObject> editColor(@Field("id[]") List<String> id,@Field("color") String color);
    @FormUrlEncoded
    @POST("checklists-color-update")
    Call<JsonObject> editcheckColor(@Field("id[]") List<String> id,@Field("color") String color);
}