package org.chat.troopschat.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;

import org.chat.troopschat.Pojo.User;

import static org.chat.troopschat.Utils.Constants.KEY_IS_LOGGEDIN;
import static org.chat.troopschat.Utils.Constants.PREF_NAME;
import static org.chat.troopschat.Utils.Constants.TOKEN;

/**
 * Created by Paras Sarna.
 */
public class Sessionmanager {
    private SharedPreferences pref;
    private Editor editor;

    public Sessionmanager(Context context) {
        pref = context.getSharedPreferences(PREF_NAME, 0);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn, User user) {
        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
        editor.putString(TOKEN, new Gson().toJson(user));
        editor.commit();
    }

    public User getToken() {
        return new Gson().fromJson(pref.getString(TOKEN, null), User.class);
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    public void clearSharedPreference() {
        editor.clear();
        editor.commit();
    }
}