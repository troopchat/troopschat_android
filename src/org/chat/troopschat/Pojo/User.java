package org.chat.troopschat.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {


    @SerializedName("error")
    @Expose
    public String error;
    @SerializedName("token")
    @Expose
    public String userId;
    @SerializedName("data")
    @Expose
    public Data data;
    public class Data {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("full_name")
        @Expose
        public String fullName;
        @SerializedName("user_name")
        @Expose
        public String userName;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("favorite_team")
        @Expose
        public String favoriteTeam;
        @SerializedName("email_verified_at")
        @Expose
        public String emailVerifiedAt;
        @SerializedName("profile_img")
        @Expose
        public String profileImg;
        @SerializedName("qrurl")
        @Expose
        public String qrurl;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;

    }
}