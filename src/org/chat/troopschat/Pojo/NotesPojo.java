package org.chat.troopschat.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotesPojo {
    @SerializedName("success")
    @Expose
    public boolean status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;

    public class Datum {

        @SerializedName("id")
        @Expose
        public String noteId;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("description")
        @Expose
        public String discription;
        @SerializedName("color")
        @Expose
        public String color;
        @SerializedName("checked_status")
        @Expose
        public int status;
        @SerializedName("reminder_date_time")
        @Expose
        public String reminderDateTime;
        @SerializedName("lock_status")
        @Expose
        public int noteLock;
        @SerializedName("user_id")
        @Expose
        public String userId;
        @SerializedName("created_at")
        @Expose
        public String createdAt;

    }
}
