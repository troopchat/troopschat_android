package org.chat.troopschat.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Members {

    @SerializedName("success")
    @Expose
    public Boolean success;
    @SerializedName("data")
    @Expose
    public Data data;

    public class Data {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("description")
        @Expose
        public Object description;
        @SerializedName("created_by")
        @Expose
        public Integer createdBy;
        @SerializedName("user_id")
        @Expose
        public Integer userId;@SerializedName("user_name")
        @Expose
        public String user_name;
        @SerializedName("trash")
        @Expose
        public String trash;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;
        @SerializedName("get_member")
        @Expose
        public List<GetMember> getMember = null;

    }
    public class GetMember {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("roster_id")
        @Expose
        public Integer rosterId;
        @SerializedName("user_name")
        @Expose
        public String user_name;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("added_by")
        @Expose
        public Integer addedBy;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;

    }
}