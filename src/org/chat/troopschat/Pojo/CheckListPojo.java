package org.chat.troopschat.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CheckListPojo {
    @SerializedName("success")
    @Expose
    public boolean status;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;

    public class Datum {

        @SerializedName("id")
        @Expose
        public String checkListId;
        @SerializedName("title")
        @Expose
        public String checkListTitle;
        @SerializedName("description")
        @Expose
        public String discription;
        @SerializedName("color")
        @Expose
        public String checkListColor;
        @SerializedName("checked_status")
        @Expose
        public int checkListStatus;
        @SerializedName("check_list_rdatetime")
        @Expose
        public String checkListRdatetime;
        @SerializedName("lock_status")
        @Expose
        public int checkListLock;
        @SerializedName("user_id")
        @Expose
        public String userId;
        @SerializedName("created_at")
        @Expose
        public String createdAt;

    }
}
