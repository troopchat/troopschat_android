package org.chat.troopschat.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CheckListItemPojo {
    @SerializedName("success")
    @Expose
    public Boolean status;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;

    public class Datum {

        @SerializedName("id")
        @Expose
        public String itemId;
        @SerializedName("title")
        @Expose
        public String itemTitle;
        @SerializedName("complete_status")
        @Expose
        public int item_status;
        @SerializedName("checklist_id")
        @Expose
        public String checklistId;
        @SerializedName("created_at")
        @Expose
        public String createdAt;

    }
}
