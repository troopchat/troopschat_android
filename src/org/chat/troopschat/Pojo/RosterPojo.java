package org.chat.troopschat.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RosterPojo {
    @SerializedName("success")
    @Expose
    public boolean status;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;
    public class Datum {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("description")
        @Expose
        public String discription;
        @SerializedName("created_by")
        @Expose
        public String createdBy;
        @SerializedName("trash")
        @Expose
        public int trashStatus;
        @SerializedName("created_at")
        @Expose
        public String createdAt;

    }
}