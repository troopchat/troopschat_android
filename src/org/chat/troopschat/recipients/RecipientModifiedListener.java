package org.chat.troopschat.recipients;


public interface RecipientModifiedListener {
  public void onModified(Recipient recipient);
}
