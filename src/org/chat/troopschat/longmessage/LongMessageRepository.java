package org.chat.troopschat.longmessage;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

import org.chat.troopschat.database.DatabaseFactory;
import org.chat.troopschat.database.MmsDatabase;
import org.chat.troopschat.database.SmsDatabase;
import org.chat.troopschat.database.model.MessageRecord;
import org.chat.troopschat.database.model.MmsMessageRecord;
import org.chat.troopschat.logging.Log;
import org.chat.troopschat.mms.PartAuthority;
import org.chat.troopschat.mms.TextSlide;
import org.chat.troopschat.util.Util;
import org.whispersystems.libsignal.util.guava.Optional;

import java.io.IOException;
import java.io.InputStream;

class LongMessageRepository {

  private final static String TAG = LongMessageRepository.class.getSimpleName();

  private final MmsDatabase mmsDatabase;
  private final SmsDatabase smsDatabase;

  LongMessageRepository(@NonNull Context context) {
    this.mmsDatabase = DatabaseFactory.getMmsDatabase(context);
    this.smsDatabase = DatabaseFactory.getSmsDatabase(context);
  }

  void getMessage(@NonNull Context context, long messageId, boolean isMms, @NonNull Callback<Optional<LongMessage>> callback) {
    AsyncTask.THREAD_POOL_EXECUTOR.execute(() -> {
      if (isMms) {
        callback.onComplete(getMmsLongMessage(context, mmsDatabase, messageId));
      } else {
        callback.onComplete(getSmsLongMessage(smsDatabase, messageId));
      }
    });
  }

  @WorkerThread
  private Optional<LongMessage> getMmsLongMessage(@NonNull Context context, @NonNull MmsDatabase mmsDatabase, long messageId) {
    Optional<MmsMessageRecord> record = getMmsMessage(mmsDatabase, messageId);

    if (record.isPresent()) {
      TextSlide textSlide = record.get().getSlideDeck().getTextSlide();

      if (textSlide != null && textSlide.getUri() != null) {
        return Optional.of(new LongMessage(record.get(), readFullBody(context, textSlide.getUri())));
      } else {
        return Optional.of(new LongMessage(record.get(), ""));
      }
    } else {
      return Optional.absent();
    }
  }

  @WorkerThread
  private Optional<LongMessage> getSmsLongMessage(@NonNull SmsDatabase smsDatabase, long messageId) {
    Optional<MessageRecord> record = getSmsMessage(smsDatabase, messageId);

    if (record.isPresent()) {
      return Optional.of(new LongMessage(record.get(), ""));
    } else {
      return Optional.absent();
    }
  }


  @WorkerThread
  private Optional<MmsMessageRecord> getMmsMessage(@NonNull MmsDatabase mmsDatabase, long messageId) {
    try (Cursor cursor = mmsDatabase.getMessage(messageId)) {
      return Optional.fromNullable((MmsMessageRecord) mmsDatabase.readerFor(cursor).getNext());
    }
  }

  @WorkerThread
  private Optional<MessageRecord> getSmsMessage(@NonNull SmsDatabase smsDatabase, long messageId) {
    try (Cursor cursor = smsDatabase.getMessageCursor(messageId)) {
      return Optional.fromNullable(smsDatabase.readerFor(cursor).getNext());
    }
  }

  private String readFullBody(@NonNull Context context, @NonNull Uri uri) {
    try (InputStream stream = PartAuthority.getAttachmentStream(context, uri)) {
      return Util.readFullyAsString(stream);
    } catch (IOException e) {
      Log.w(TAG, "Failed to read full text body.", e);
      return "";
    }
  }

  interface Callback<T> {
    void onComplete(T result);
  }
}
