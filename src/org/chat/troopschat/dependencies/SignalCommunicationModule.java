package org.chat.troopschat.dependencies;

import android.content.Context;

import org.chat.troopschat.gcm.FcmService;
import org.chat.troopschat.jobs.AttachmentUploadJob;
import org.chat.troopschat.jobs.MultiDeviceConfigurationUpdateJob;
import org.chat.troopschat.jobs.RefreshUnidentifiedDeliveryAbilityJob;
import org.chat.troopschat.jobs.RotateProfileKeyJob;
import org.chat.troopschat.jobs.TypingSendJob;
import org.chat.troopschat.logging.Log;

import org.greenrobot.eventbus.EventBus;
import org.chat.troopschat.BuildConfig;
import org.chat.troopschat.CreateProfileActivity;
import org.chat.troopschat.DeviceListFragment;
import org.chat.troopschat.crypto.storage.SignalProtocolStoreImpl;
import org.chat.troopschat.events.ReminderUpdateEvent;
import org.chat.troopschat.jobs.AttachmentDownloadJob;
import org.chat.troopschat.jobs.AvatarDownloadJob;
import org.chat.troopschat.jobs.CleanPreKeysJob;
import org.chat.troopschat.jobs.CreateSignedPreKeyJob;
import org.chat.troopschat.jobs.FcmRefreshJob;
import org.chat.troopschat.jobs.MultiDeviceBlockedUpdateJob;
import org.chat.troopschat.jobs.MultiDeviceContactUpdateJob;
import org.chat.troopschat.jobs.MultiDeviceGroupUpdateJob;
import org.chat.troopschat.jobs.MultiDeviceProfileKeyUpdateJob;
import org.chat.troopschat.jobs.MultiDeviceReadUpdateJob;
import org.chat.troopschat.jobs.MultiDeviceVerifiedUpdateJob;
import org.chat.troopschat.jobs.PushGroupSendJob;
import org.chat.troopschat.jobs.PushGroupUpdateJob;
import org.chat.troopschat.jobs.PushMediaSendJob;
import org.chat.troopschat.jobs.PushNotificationReceiveJob;
import org.chat.troopschat.jobs.PushTextSendJob;
import org.chat.troopschat.jobs.RefreshAttributesJob;
import org.chat.troopschat.jobs.RefreshPreKeysJob;
import org.chat.troopschat.jobs.RequestGroupInfoJob;
import org.chat.troopschat.jobs.RetrieveProfileAvatarJob;
import org.chat.troopschat.jobs.RetrieveProfileJob;
import org.chat.troopschat.jobs.RotateCertificateJob;
import org.chat.troopschat.jobs.RotateSignedPreKeyJob;
import org.chat.troopschat.jobs.SendDeliveryReceiptJob;
import org.chat.troopschat.jobs.SendReadReceiptJob;
import org.chat.troopschat.preferences.AppProtectionPreferenceFragment;
import org.chat.troopschat.push.SecurityEventListener;
import org.chat.troopschat.push.SignalServiceNetworkAccess;
import org.chat.troopschat.service.IncomingMessageObserver;
import org.chat.troopschat.service.WebRtcCallService;
import org.chat.troopschat.util.TextSecurePreferences;
import org.whispersystems.libsignal.util.guava.Optional;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.SignalServiceMessageReceiver;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.util.CredentialsProvider;
import org.whispersystems.signalservice.api.util.RealtimeSleepTimer;
import org.whispersystems.signalservice.api.util.SleepTimer;
import org.whispersystems.signalservice.api.util.UptimeSleepTimer;
import org.whispersystems.signalservice.api.websocket.ConnectivityListener;

import dagger.Module;
import dagger.Provides;

@Module(complete = false, injects = {CleanPreKeysJob.class,
                                     CreateSignedPreKeyJob.class,
                                     PushGroupSendJob.class,
                                     PushTextSendJob.class,
                                     PushMediaSendJob.class,
                                     AttachmentDownloadJob.class,
                                     RefreshPreKeysJob.class,
                                     IncomingMessageObserver.class,
                                     PushNotificationReceiveJob.class,
                                     MultiDeviceContactUpdateJob.class,
                                     MultiDeviceGroupUpdateJob.class,
                                     MultiDeviceReadUpdateJob.class,
                                     MultiDeviceBlockedUpdateJob.class,
                                     DeviceListFragment.class,
                                     RefreshAttributesJob.class,
                                     FcmRefreshJob.class,
                                     RequestGroupInfoJob.class,
                                     PushGroupUpdateJob.class,
                                     AvatarDownloadJob.class,
                                     RotateSignedPreKeyJob.class,
                                     WebRtcCallService.class,
                                     RetrieveProfileJob.class,
                                     MultiDeviceVerifiedUpdateJob.class,
                                     CreateProfileActivity.class,
                                     RetrieveProfileAvatarJob.class,
                                     MultiDeviceProfileKeyUpdateJob.class,
                                     SendReadReceiptJob.class,
                                     AppProtectionPreferenceFragment.class,
                                     FcmService.class,
                                     RotateCertificateJob.class,
                                     SendDeliveryReceiptJob.class,
                                     RotateProfileKeyJob.class,
                                     MultiDeviceConfigurationUpdateJob.class,
                                     RefreshUnidentifiedDeliveryAbilityJob.class,
                                     TypingSendJob.class,
                                     AttachmentUploadJob.class})
public class SignalCommunicationModule {

  private static final String TAG = SignalCommunicationModule.class.getSimpleName();

  private final Context                      context;
  private final SignalServiceNetworkAccess   networkAccess;

  private SignalServiceAccountManager  accountManager;
  private SignalServiceMessageSender   messageSender;
  private SignalServiceMessageReceiver messageReceiver;

  public SignalCommunicationModule(Context context, SignalServiceNetworkAccess networkAccess) {
    this.context       = context;
    this.networkAccess = networkAccess;
  }

  @Provides
  synchronized SignalServiceAccountManager provideSignalAccountManager() {
    if (this.accountManager == null) {
      this.accountManager = new SignalServiceAccountManager(networkAccess.getConfiguration(context),
                                                            new DynamicCredentialsProvider(context),
                                                            BuildConfig.USER_AGENT);
    }

    return this.accountManager;
  }

  @Provides
  synchronized SignalServiceMessageSender provideSignalMessageSender() {
    if (this.messageSender == null) {
      this.messageSender = new SignalServiceMessageSender(networkAccess.getConfiguration(context),
                                                          new DynamicCredentialsProvider(context),
                                                          new SignalProtocolStoreImpl(context),
                                                          BuildConfig.USER_AGENT,
                                                          TextSecurePreferences.isMultiDevice(context),
                                                          Optional.fromNullable(IncomingMessageObserver.getPipe()),
                                                          Optional.fromNullable(IncomingMessageObserver.getUnidentifiedPipe()),
                                                          Optional.of(new SecurityEventListener(context)));
    } else {
      this.messageSender.setMessagePipe(IncomingMessageObserver.getPipe(), IncomingMessageObserver.getUnidentifiedPipe());
      this.messageSender.setIsMultiDevice(TextSecurePreferences.isMultiDevice(context));
    }

    return this.messageSender;
  }

  @Provides
  synchronized SignalServiceMessageReceiver provideSignalMessageReceiver() {
    if (this.messageReceiver == null) {
      SleepTimer sleepTimer =  TextSecurePreferences.isFcmDisabled(context) ? new RealtimeSleepTimer(context) : new UptimeSleepTimer();

      this.messageReceiver = new SignalServiceMessageReceiver(networkAccess.getConfiguration(context),
                                                              new DynamicCredentialsProvider(context),
                                                              BuildConfig.USER_AGENT,
                                                              new PipeConnectivityListener(),
                                                              sleepTimer);
    }

    return this.messageReceiver;
  }

  @Provides
  synchronized SignalServiceNetworkAccess provideSignalServiceNetworkAccess() {
    return networkAccess;
  }

  private static class DynamicCredentialsProvider implements CredentialsProvider {

    private final Context context;

    private DynamicCredentialsProvider(Context context) {
      this.context = context.getApplicationContext();
    }

    @Override
    public String getUser() {
      return TextSecurePreferences.getLocalNumber(context);
    }

    @Override
    public String getPassword() {
      return TextSecurePreferences.getPushServerPassword(context);
    }

    @Override
    public String getSignalingKey() {
      return TextSecurePreferences.getSignalingKey(context);
    }
  }

  private class PipeConnectivityListener implements ConnectivityListener {

    @Override
    public void onConnected() {
      Log.i(TAG, "onConnected()");
    }

    @Override
    public void onConnecting() {
      Log.i(TAG, "onConnecting()");
    }

    @Override
    public void onDisconnected() {
      Log.w(TAG, "onDisconnected()");
    }

    @Override
    public void onAuthenticationFailure() {
      Log.w(TAG, "onAuthenticationFailure()");
      TextSecurePreferences.setUnauthorizedReceived(context, true);
      EventBus.getDefault().post(new ReminderUpdateEvent());
    }

  }

}
