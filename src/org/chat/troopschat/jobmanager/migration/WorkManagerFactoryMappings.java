package org.chat.troopschat.jobmanager.migration;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.chat.troopschat.jobs.AttachmentDownloadJob;
import org.chat.troopschat.jobs.AttachmentUploadJob;
import org.chat.troopschat.jobs.AvatarDownloadJob;
import org.chat.troopschat.jobs.CleanPreKeysJob;
import org.chat.troopschat.jobs.CreateSignedPreKeyJob;
import org.chat.troopschat.jobs.DirectoryRefreshJob;
import org.chat.troopschat.jobs.FcmRefreshJob;
import org.chat.troopschat.jobs.LocalBackupJob;
import org.chat.troopschat.jobs.MmsDownloadJob;
import org.chat.troopschat.jobs.MmsReceiveJob;
import org.chat.troopschat.jobs.MmsSendJob;
import org.chat.troopschat.jobs.MultiDeviceBlockedUpdateJob;
import org.chat.troopschat.jobs.MultiDeviceConfigurationUpdateJob;
import org.chat.troopschat.jobs.MultiDeviceContactUpdateJob;
import org.chat.troopschat.jobs.MultiDeviceGroupUpdateJob;
import org.chat.troopschat.jobs.MultiDeviceProfileKeyUpdateJob;
import org.chat.troopschat.jobs.MultiDeviceReadUpdateJob;
import org.chat.troopschat.jobs.MultiDeviceVerifiedUpdateJob;
import org.chat.troopschat.jobs.PushContentReceiveJob;
import org.chat.troopschat.jobs.PushDecryptJob;
import org.chat.troopschat.jobs.PushGroupSendJob;
import org.chat.troopschat.jobs.PushGroupUpdateJob;
import org.chat.troopschat.jobs.PushMediaSendJob;
import org.chat.troopschat.jobs.PushNotificationReceiveJob;
import org.chat.troopschat.jobs.PushTextSendJob;
import org.chat.troopschat.jobs.RefreshAttributesJob;
import org.chat.troopschat.jobs.RefreshPreKeysJob;
import org.chat.troopschat.jobs.RefreshUnidentifiedDeliveryAbilityJob;
import org.chat.troopschat.jobs.RequestGroupInfoJob;
import org.chat.troopschat.jobs.RetrieveProfileAvatarJob;
import org.chat.troopschat.jobs.RetrieveProfileJob;
import org.chat.troopschat.jobs.RotateCertificateJob;
import org.chat.troopschat.jobs.RotateProfileKeyJob;
import org.chat.troopschat.jobs.RotateSignedPreKeyJob;
import org.chat.troopschat.jobs.SendDeliveryReceiptJob;
import org.chat.troopschat.jobs.SendReadReceiptJob;
import org.chat.troopschat.jobs.ServiceOutageDetectionJob;
import org.chat.troopschat.jobs.SmsReceiveJob;
import org.chat.troopschat.jobs.SmsSendJob;
import org.chat.troopschat.jobs.SmsSentJob;
import org.chat.troopschat.jobs.TrimThreadJob;
import org.chat.troopschat.jobs.TypingSendJob;
import org.chat.troopschat.jobs.UpdateApkJob;

import java.util.HashMap;
import java.util.Map;

public class WorkManagerFactoryMappings {

  private static final Map<String, String> FACTORY_MAP = new HashMap<String, String>() {{
    put(AttachmentDownloadJob.class.getName(), AttachmentDownloadJob.KEY);
    put(AttachmentUploadJob.class.getName(), AttachmentUploadJob.KEY);
    put(AvatarDownloadJob.class.getName(), AvatarDownloadJob.KEY);
    put(CleanPreKeysJob.class.getName(), CleanPreKeysJob.KEY);
    put(CreateSignedPreKeyJob.class.getName(), CreateSignedPreKeyJob.KEY);
    put(DirectoryRefreshJob.class.getName(), DirectoryRefreshJob.KEY);
    put(FcmRefreshJob.class.getName(), FcmRefreshJob.KEY);
    put(LocalBackupJob.class.getName(), LocalBackupJob.KEY);
    put(MmsDownloadJob.class.getName(), MmsDownloadJob.KEY);
    put(MmsReceiveJob.class.getName(), MmsReceiveJob.KEY);
    put(MmsSendJob.class.getName(), MmsSendJob.KEY);
    put(MultiDeviceBlockedUpdateJob.class.getName(), MultiDeviceBlockedUpdateJob.KEY);
    put(MultiDeviceConfigurationUpdateJob.class.getName(), MultiDeviceConfigurationUpdateJob.KEY);
    put(MultiDeviceContactUpdateJob.class.getName(), MultiDeviceContactUpdateJob.KEY);
    put(MultiDeviceGroupUpdateJob.class.getName(), MultiDeviceGroupUpdateJob.KEY);
    put(MultiDeviceProfileKeyUpdateJob.class.getName(), MultiDeviceProfileKeyUpdateJob.KEY);
    put(MultiDeviceReadUpdateJob.class.getName(), MultiDeviceReadUpdateJob.KEY);
    put(MultiDeviceVerifiedUpdateJob.class.getName(), MultiDeviceVerifiedUpdateJob.KEY);
    put(PushContentReceiveJob.class.getName(), PushContentReceiveJob.KEY);
    put(PushDecryptJob.class.getName(), PushDecryptJob.KEY);
    put(PushGroupSendJob.class.getName(), PushGroupSendJob.KEY);
    put(PushGroupUpdateJob.class.getName(), PushGroupUpdateJob.KEY);
    put(PushMediaSendJob.class.getName(), PushMediaSendJob.KEY);
    put(PushNotificationReceiveJob.class.getName(), PushNotificationReceiveJob.KEY);
    put(PushTextSendJob.class.getName(), PushTextSendJob.KEY);
    put(RefreshAttributesJob.class.getName(), RefreshAttributesJob.KEY);
    put(RefreshPreKeysJob.class.getName(), RefreshPreKeysJob.KEY);
    put(RefreshUnidentifiedDeliveryAbilityJob.class.getName(), RefreshUnidentifiedDeliveryAbilityJob.KEY);
    put(RequestGroupInfoJob.class.getName(), RequestGroupInfoJob.KEY);
    put(RetrieveProfileAvatarJob.class.getName(), RetrieveProfileAvatarJob.KEY);
    put(RetrieveProfileJob.class.getName(), RetrieveProfileJob.KEY);
    put(RotateCertificateJob.class.getName(), RotateCertificateJob.KEY);
    put(RotateProfileKeyJob.class.getName(), RotateProfileKeyJob.KEY);
    put(RotateSignedPreKeyJob.class.getName(), RotateSignedPreKeyJob.KEY);
    put(SendDeliveryReceiptJob.class.getName(), SendDeliveryReceiptJob.KEY);
    put(SendReadReceiptJob.class.getName(), SendReadReceiptJob.KEY);
    put(ServiceOutageDetectionJob.class.getName(), ServiceOutageDetectionJob.KEY);
    put(SmsReceiveJob.class.getName(), SmsReceiveJob.KEY);
    put(SmsSendJob.class.getName(), SmsSendJob.KEY);
    put(SmsSentJob.class.getName(), SmsSentJob.KEY);
    put(TrimThreadJob.class.getName(), TrimThreadJob.KEY);
    put(TypingSendJob.class.getName(), TypingSendJob.KEY);
    put(UpdateApkJob.class.getName(), UpdateApkJob.KEY);
  }};

  public static @Nullable String getFactoryKey(@NonNull String workManagerClass) {
    return FACTORY_MAP.get(workManagerClass);
  }
}
