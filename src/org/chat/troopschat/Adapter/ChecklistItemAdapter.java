package org.chat.troopschat.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.chat.troopschat.Activities.CheckListDetail;
import org.chat.troopschat.Pojo.CheckListItemPojo;
import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChecklistItemAdapter extends RecyclerView.Adapter<ChecklistItemAdapter.MyViewHolder> {
    BottomSheetDialog sheetBehavior;
    EditText note_et, title_et;
    ImageView close;
    TextView title_tv, create_tv;
    ArrayList<String> ids = new ArrayList<>();
    ActionMode actionmode;
    boolean isMultiSelect;
    LinearLayout toolbar;
    private Context context;
    private List<CheckListItemPojo.Datum> itemList;

    public ChecklistItemAdapter(Context context, LinearLayout toolbar, List<CheckListItemPojo.Datum> itemList) {
        this.context = context;
        this.itemList = itemList;
        this.toolbar = toolbar;
        sheetBehavior = new BottomSheetDialog(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.checklistitem_rv, viewGroup, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        if (!isMultiSelect) {
            myViewHolder.delete.setVisibility(View.VISIBLE);
            myViewHolder.check.setVisibility(View.VISIBLE);
        } else {
            myViewHolder.delete.setVisibility(View.GONE);
            myViewHolder.check.setVisibility(View.GONE);
        }
        myViewHolder.name_tv.setText(itemList.get(i).itemTitle);
        if (itemList.get(i).item_status==1)
            myViewHolder.name_tv.setPaintFlags(myViewHolder.name_tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    private void editItem(int pos, TextView name) {
        ((CheckListDetail) context).service.updatelistItem(itemList.get(pos).itemId, note_et != null ? note_et.getText().toString().trim() : name.getText().toString(), itemList.get(pos).item_status).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                CommonUtills.progrescancel();
                if (response.body() != null) {
                    if (response.body().get("success").getAsBoolean()) {
                        ((CheckListDetail) context).getItems();
                        sheetBehavior.dismiss();
                    }
                } else
                    CommonUtills.alertDialog(context, "Something went Wrong Please try again");
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                CommonUtills.progrescancel();
                CommonUtills.alertDialog(context, "Something went Wrong Please try again");
            }
        });
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name_tv, date_tv;
        ImageView delete, check;

        MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            name_tv = itemView.findViewById(R.id.name_tv);
            date_tv = itemView.findViewById(R.id.date_tv);
            delete = itemView.findViewById(R.id.delete);
            check = itemView.findViewById(R.id.check);
            delete.setOnClickListener(this);
            check.setOnClickListener(this);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isMultiSelect) {
                        if (ids.contains(itemList.get(getAdapterPosition()).itemId)) {
                            ids.remove(itemList.get(getAdapterPosition()).itemId);
                            itemView.setBackgroundColor(Color.WHITE);
                        } else {
                            ids.add(itemList.get(getAdapterPosition()).itemId);
                            itemView.setBackgroundColor(Color.parseColor("#A9A9A9"));
                        }
                    } else {
                        itemList.get(getAdapterPosition()).item_status = itemList.get(getAdapterPosition()).item_status==1 ? 0 : 1;
                        editItem(getAdapterPosition(), name_tv);
                    }
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    isMultiSelect = true;
                    ids.add(itemList.get(getAdapterPosition()).itemId);
                    itemView.setBackgroundColor(Color.parseColor("#A9A9A9"));
                    actionmode = ((AppCompatActivity) context).startActionMode(new android.view.ActionMode.Callback() {
                        @Override
                        public boolean onCreateActionMode(android.view.ActionMode mode, Menu menu) {
                            MenuInflater menuInflater = mode.getMenuInflater();
                            menuInflater.inflate(R.menu.actionmenu, menu);
                            menu.getItem(1).setVisible(false);
                            menu.getItem(2).setVisible(false);
                            menu.getItem(3).setVisible(false);
                            toolbar.setVisibility(View.GONE);
                            notifyDataSetChanged();
                            return true;
                        }

                        @Override
                        public boolean onPrepareActionMode(android.view.ActionMode mode, Menu menu) {
                            return false;
                        }

                        @Override
                        public boolean onActionItemClicked(android.view.ActionMode mode, MenuItem item) {
                            int id = item.getItemId();
                            if (id == R.id.delete) {
                                deleteItems();
                                return true;
                            }
                            return true;
                        }

                        @Override
                        public void onDestroyActionMode(android.view.ActionMode mode) {
                            toolbar.setVisibility(View.VISIBLE);
                            ((CheckListDetail) context).getItems();
                        }
                    });
                    return true;
                }
            });
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.check) {
                v = ((Activity) context).getLayoutInflater().inflate(R.layout.noteadd_layout, null);
                sheetBehavior.setContentView(v);
                close = sheetBehavior.findViewById(R.id.close);
                title_et = sheetBehavior.findViewById(R.id.title_et);
                note_et = sheetBehavior.findViewById(R.id.note_et);
                title_tv = sheetBehavior.findViewById(R.id.title_tv);
                title_tv.setText("Update Item");
                title_et.setVisibility(View.GONE);
                note_et.setHint("Enter Item");
                note_et.setText(itemList.get(getAdapterPosition()).itemTitle);
                create_tv = sheetBehavior.findViewById(R.id.create_tv);
                close.setOnClickListener(MyViewHolder.this);
                create_tv.setOnClickListener(MyViewHolder.this);
                sheetBehavior.show();
            } else if (v.getId() == R.id.close) {
                sheetBehavior.dismiss();
            } else if (v.getId() == R.id.create_tv) {
                editItem(getAdapterPosition(), name_tv);
            } else if (v.getId() == R.id.delete) {
                ids.add(itemList.get(getAdapterPosition()).itemId);
                deleteItems();

            }
        }

        private void deleteItems() {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Are you Sure?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ((CheckListDetail) context).service.delechecklistItem(ids).enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                            CommonUtills.progrescancel();
                            if (response.body() != null) {
                                if (response.body().get("success").getAsBoolean()) {
                                    if (actionmode != null)
                                        actionmode.finish();
                                    else ((CheckListDetail) context).getItems();
                                }
                            } else
                                CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                        }

                        @Override
                        public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                            CommonUtills.progrescancel();
                            CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                        }
                    });
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }
}