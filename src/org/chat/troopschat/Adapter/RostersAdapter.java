package org.chat.troopschat.Adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.chat.troopschat.Activities.RosterDetail;
import org.chat.troopschat.Activities.Rosters;
import org.chat.troopschat.Pojo.RosterPojo;
import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RostersAdapter extends RecyclerView.Adapter<RostersAdapter.MyViewHolder> {
    ArrayList<String> ids = new ArrayList<>();
    ActionMode actionmode;
    boolean isMultiSelect;
    View toolbar;
    List<String> colors = new ArrayList<>();
    boolean all, trash, recent, shared;
    private Context context;
    private List<RosterPojo.Datum> itemList;

    public RostersAdapter(Context context, View toolbar, List<RosterPojo.Datum> itemList, boolean all, boolean recent, boolean shared, boolean trash) {
        this.context = context;
        this.itemList = itemList;
        this.trash = trash;
        this.all = all;
        this.recent = recent;
        this.shared = shared;
        this.toolbar = toolbar;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rostersslist_rv, viewGroup, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        if (trash && !isMultiSelect) {
            myViewHolder.delete.setVisibility(View.VISIBLE);
            myViewHolder.restore.setVisibility(View.VISIBLE);
        } else {
            myViewHolder.delete.setVisibility(View.GONE);
            myViewHolder.restore.setVisibility(View.GONE);
        }
        myViewHolder.date_tv.setText(itemList.get(i).createdAt);
        myViewHolder.name_tv.setText(itemList.get(i).title);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    private void delete() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you Sure?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((Rosters) context).service.trashRoster(ids).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                        if (response.body() != null) {
                            if (response.body().get("success").getAsBoolean()) {
                                if (actionmode != null)
                                    actionmode.finish();

                                        ((Rosters) context).setData();
                                }
                            }
                         else
                            CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                    }

                    @Override
                    public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                        CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                    }
                });
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void deletetrash() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you Sure?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((Rosters) context).service.deleteRoster(ids).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                        if (response.body() != null) {
                            if (response.body().get("success").getAsBoolean()) {
                                if (actionmode != null)
                                    actionmode.finish();
                                else {
                                    if (!trash)
                                        ((Rosters) context).setData();
                                    else
                                        ((Rosters) context).setTrashData();
                                }
                            }
                        } else
                            CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                    }

                    @Override
                    public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                        CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                    }
                });
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showDialog() {
        final Dialog dialog;
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        LayoutInflater inflater = ((AppCompatActivity) context).getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.color_popup, null);
        alert.setView(alertLayout);
        alert.setCancelable(false);
        dialog = alert.create();
        GridView gridView = alertLayout.findViewById(R.id.attributes_list);
        gridView.setVisibility(View.VISIBLE);
        gridView.setAdapter(new ColorsAdapter(context, "note", colors, ids, actionmode, dialog));
        alertLayout.findViewById(R.id.cancel_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name_tv, date_tv;
        ImageView delete, restore;
        CardView main_cv;

        MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            name_tv = itemView.findViewById(R.id.name_tv);
            restore = itemView.findViewById(R.id.restore);
            date_tv = itemView.findViewById(R.id.date_tv);
            main_cv = itemView.findViewById(R.id.main_cv);
            delete = itemView.findViewById(R.id.delete);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isMultiSelect) {
                        if (ids.contains(itemList.get(getAdapterPosition()).id)) {
                            ids.remove(itemList.get(getAdapterPosition()).id);
                            itemView.setBackgroundColor(Color.WHITE);
                        } else {
                            ids.add(itemList.get(getAdapterPosition()).id);
                            itemView.setBackgroundColor(Color.parseColor("#A9A9A9"));
                        }
                    } else if (!trash) {
                        Intent intent = new Intent(context, RosterDetail.class);
                        intent.putExtra("title", itemList.get(getAdapterPosition()).title);
                        intent.putExtra("desc", itemList.get(getAdapterPosition()).discription);
                        intent.putExtra("createdby", itemList.get(getAdapterPosition()).createdBy);
                        intent.putExtra("id", itemList.get(getAdapterPosition()).id);
                        context.startActivity(intent);
                    }
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    isMultiSelect = true;
                    ids.add(itemList.get(getAdapterPosition()).id);
                    itemView.setBackgroundColor(Color.parseColor("#A9A9A9"));
                    actionmode = ((AppCompatActivity) context).startActionMode(new ActionMode.Callback() {
                        @Override
                        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                            MenuInflater menuInflater = mode.getMenuInflater();
                            menuInflater.inflate(R.menu.actionmenu, menu);
                            menu.getItem(2).setVisible(false);
                            menu.getItem(1).setVisible(false);
                            if (!trash)
                                menu.getItem(3).setVisible(false);
                            toolbar.setVisibility(View.GONE);
                            if (trash)
                                notifyDataSetChanged();
                            return true;
                        }

                        @Override
                        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                            return false;
                        }

                        @Override
                        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                            int id = item.getItemId();
                            if (id == R.id.delete) {
                                if (!trash)
                                    delete();
                                else
                                    deletetrash();
                                return true;
                            } else if (id == R.id.color) {
                                showDialog();
                            } else if (id == R.id.restore) {
                                restoreItems();
                            }
                            return true;
                        }

                        @Override
                        public void onDestroyActionMode(ActionMode mode) {
                            toolbar.setVisibility(View.VISIBLE);
                            if (trash)
                                ((Rosters) context).setTrashData();
                            else if (recent)
                                ((Rosters) context).setrecentData();
                            else if (shared)
                                ((Rosters) context).setSharedData();
                            else
                                ((Rosters) context).setData();
                        }
                    });
                    return true;
                }
            });
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ids.add(itemList.get(getAdapterPosition()).id);
                    deletetrash();
                }
            });
            restore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ids.add(itemList.get(getAdapterPosition()).id);
                    restoreItems();
                }
            });
        }

        private void restoreItems() {
            ((Rosters) context).service.restorerostertrash(ids).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                    if (response.body() != null) {
                        if (actionmode != null)
                            actionmode.finish();
                        else
                            ((Rosters) context).setTrashData();
                    } else
                        CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                }

                @Override
                public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                    CommonUtills.progrescancel();
                    CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                }
            });
        }
    }
}