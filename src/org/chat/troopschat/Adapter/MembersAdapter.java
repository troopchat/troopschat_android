package org.chat.troopschat.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.chat.troopschat.Activities.RosterDetail;
import org.chat.troopschat.Pojo.Members;
import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MembersAdapter extends RecyclerView.Adapter<MembersAdapter.MyViewHolder> {
    String id;
    ArrayList<String> ids = new ArrayList<>();
    ActionMode actionmode;
    boolean isMultiSelect;
    View toolbar;
    private Context context;
    private List<Members.GetMember> itemList;

    public MembersAdapter(Context context, String id, List<Members.GetMember> itemList, View toolbar) {
        this.context = context;
        this.itemList = itemList;
        this.toolbar = toolbar;
        this.id = id;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.members_rv, viewGroup, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        if (isMultiSelect)
            myViewHolder.delete.setVisibility(View.GONE);
        myViewHolder.name_tv.setText(itemList.get(i).user_name);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name_tv;
        ImageView delete;

        MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            name_tv = itemView.findViewById(R.id.name_tv);
            delete = itemView.findViewById(R.id.delete);
            delete.setOnClickListener(this);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isMultiSelect) {
                        if (ids.contains(itemList.get(getAdapterPosition()).id.toString())) {
                            ids.remove(itemList.get(getAdapterPosition()).id.toString());
                            itemView.setBackgroundColor(Color.WHITE);
                        } else {
                            ids.add(itemList.get(getAdapterPosition()).id.toString());
                            itemView.setBackgroundColor(Color.parseColor("#A9A9A9"));
                        }
                    }
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    isMultiSelect = true;
                    ids.add(itemList.get(getAdapterPosition()).id.toString());
                    itemView.setBackgroundColor(Color.parseColor("#A9A9A9"));
                    actionmode = ((AppCompatActivity) context).startActionMode(new ActionMode.Callback() {
                        @Override
                        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                            MenuInflater menuInflater = mode.getMenuInflater();
                            menuInflater.inflate(R.menu.actionmenu, menu);
                            menu.getItem(1).setVisible(false);
                            menu.getItem(2).setVisible(false);
                            menu.getItem(3).setVisible(false);
                            toolbar.setVisibility(View.GONE);
                            notifyDataSetChanged();
                            return true;
                        }

                        @Override
                        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                            return false;
                        }

                        @Override
                        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                            int id = item.getItemId();
                            if (id == R.id.delete) {
                                deleteItems();
                                return true;
                            }
                            return true;
                        }

                        @Override
                        public void onDestroyActionMode(ActionMode mode) {
                            toolbar.setVisibility(View.VISIBLE);
                            ((RosterDetail) context).getMembers();
                        }
                    });
                    return true;
                }
            });
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.delete) {
                ids.add(itemList.get(getAdapterPosition()).id.toString());
                deleteItems();
            }
        }

        private void deleteItems() {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Are you Sure?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    CommonUtills.progresshow(context);

                    ((RosterDetail) context).service.deleterostermember(ids).enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                            CommonUtills.progrescancel();
                            if (response.body() != null) {
                                if (response.body().get("success").getAsBoolean()) {
                                    if (actionmode != null)
                                        actionmode.finish();
                                    else ((RosterDetail) context).getMembers();
                                }
                                CommonUtills.alertDialog(context, response.body().get("message").getAsString());
                            } else
                                CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                        }

                        @Override
                        public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                            CommonUtills.progrescancel();
                            CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                        }
                    });
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }
}