package org.chat.troopschat.Adapter;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Patterns;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.chat.troopschat.Activities.Files;
import org.chat.troopschat.Pojo.FilesPojo;
import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;
import org.chat.troopschat.Utils.CustomRecyclerView;
import org.chat.troopschat.Utils.Interfaces.Folderclick;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.DOWNLOAD_SERVICE;
import static android.os.Environment.DIRECTORY_DOWNLOADS;


public class FilesAdapter extends RecyclerView.Adapter<FilesAdapter.MyViewHolder> {
    ArrayList<String> ids;
    ArrayList<Integer> types = new ArrayList<>();
    ArrayList<String> urls;
    ActionMode actionmode;
    boolean isMultiSelect;
    View toolbar;
    private Context context;
    private List<FilesPojo> itemList;
    private boolean isList, folderdata, all, recent, shared;
    private BottomSheetDialog sheetBehavior;
    private ImageView close;
    private LinearLayout share_ln, delete_ln, link_ll, mail_ll, rename_ln, other_ll, move_ln, option_ln, download_ln, rename_ll;
    private TextView send_tv, rename_tv, cancel_tv;
    private EditText email_et, message_et;
    private Folderclick folderclick;

    public FilesAdapter(Context context, View toolbar, List<FilesPojo> itemList, boolean isList, BottomSheetDialog sheetBehavior, Folderclick folderclick, boolean folderdata, boolean all, boolean recent, boolean shared) {
        this.itemList = itemList;
        this.context = context;
        this.sheetBehavior = sheetBehavior;
        this.isList = isList;
        this.folderclick = folderclick;
        this.folderdata = folderdata;
        this.all = all;
        this.toolbar = toolbar;
        this.recent = recent;
        this.shared = shared;
        setHasStableIds(true);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(isList ? R.layout.fileslist_rv : R.layout.filesgrid_rv, viewGroup, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.date_tv.setText(itemList.get(i).date);
        myViewHolder.name_tv.setText(itemList.get(i).name);
        if (itemList.get(i).type == 0)
            myViewHolder.file_iv.setImageResource(R.drawable.folder);
        else
            myViewHolder.file_iv.setImageResource(R.drawable.file);
        if (isMultiSelect)
            myViewHolder.more_iv.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        if (actionmode != null)
            actionmode.finish();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView file_iv, more_iv;
        TextView name_tv, date_tv;

        MyViewHolder(final View itemView) {
            super(itemView);
            file_iv = itemView.findViewById(R.id.file_iv);
            name_tv = itemView.findViewById(R.id.name_tv);
            date_tv = itemView.findViewById(R.id.date_tv);
            more_iv = itemView.findViewById(R.id.more_iv);
            more_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDialog();
                    if (itemList.get(getAdapterPosition()).type == 0) {
                        move_ln.setVisibility(View.GONE);
                        download_ln.setVisibility(View.GONE);

                    }
                    if(shared) {
                        share_ln.setVisibility(View.GONE);
                        rename_ln.setVisibility(View.GONE);
                    }
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    isMultiSelect = true;
                    ids = new ArrayList<>();
                    urls = new ArrayList<>();
                    ids.add(itemList.get(getAdapterPosition()).id);
                    types.add(itemList.get(getAdapterPosition()).type);
                    urls.add(itemList.get(getAdapterPosition()).url);
                    itemView.setBackgroundColor(Color.parseColor("#A9A9A9"));
                    actionmode = ((AppCompatActivity) context).startActionMode(new android.view.ActionMode.Callback() {
                        @Override
                        public boolean onCreateActionMode(android.view.ActionMode mode, Menu menu) {
                            MenuInflater menuInflater = mode.getMenuInflater();
                            menuInflater.inflate(R.menu.actionmenu, menu);
                            menu.getItem(0).setVisible(false);
                            menu.getItem(1).setVisible(false);
                            menu.getItem(3).setVisible(false);
                            toolbar.setVisibility(View.GONE);
                            return true;
                        }

                        @Override
                        public boolean onPrepareActionMode(android.view.ActionMode mode, Menu menu) {
                            return false;
                        }

                        @Override
                        public boolean onActionItemClicked(android.view.ActionMode mode, MenuItem item) {
                            int id = item.getItemId();
                            if (id == R.id.more) {
                                showDialog();
                                rename_ln.setVisibility(View.GONE);
                                if(shared){
                                    share_ln.setVisibility(View.GONE);
                                    rename_ln.setVisibility(View.GONE);
                                }
                                return true;
                            }
                            return true;
                        }

                        @Override
                        public void onDestroyActionMode(android.view.ActionMode mode) {
                            toolbar.setVisibility(View.VISIBLE);
                            if (all) {
                                if (folderdata)
                                    ((Files) context).setFolderData();
                                else
                                    ((Files) context).setData();
                            } else if (recent) {
                                ((Files) context).setrecentData();
                            } else {
                                ((Files) context).setSharedData();
                            }
                        }
                    });
                    notifyDataSetChanged();
                    return true;
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isMultiSelect) {
                        if (types.contains(itemList.get(getAdapterPosition()).type)) {
                            if (ids.contains(itemList.get(getAdapterPosition()).id)) {
                                ids.remove(itemList.get(getAdapterPosition()).id);
                                types.remove(itemList.get(getAdapterPosition()).type);
                                urls.remove(itemList.get(getAdapterPosition()).url);
                                itemView.setBackgroundColor(Color.WHITE);
                            } else {
                                ids.add(itemList.get(getAdapterPosition()).id);
                                types.add(itemList.get(getAdapterPosition()).type);
                                urls.add(itemList.get(getAdapterPosition()).url);
                                itemView.setBackgroundColor(Color.parseColor("#A9A9A9"));
                            }
                        } else {
                            Toast.makeText(context, "You can't select folder and file at same time", Toast.LENGTH_SHORT).show();
                        }
                    } else if (itemList.get(getAdapterPosition()).type == 0)
                        folderclick.click(itemList.get(getAdapterPosition()).id);
                }
            });
        }

        private void saveFile() {
            if (urls == null) {
                urls = new ArrayList<>();
                urls.add(itemList.get(getAdapterPosition()).url);
            }
            for (int i = 0; i < urls.size(); i++) {
                try {
                    String date = DateFormat.getDateTimeInstance().format(new Date());
                    String file = context.getString(R.string.app_name) + date.replace(" ", "").replace(":", "").replace(".", "")+"." + itemList.get(getAdapterPosition()).extension;
                    DownloadManager dm = (DownloadManager) context.getSystemService(DOWNLOAD_SERVICE);
                    Uri downloadUri = Uri.parse(urls.get(i));
                    DownloadManager.Request request = new DownloadManager.Request(downloadUri);
                    request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                            .setDestinationInExternalPublicDir(DIRECTORY_DOWNLOADS + File.separator, file)
                            .setTitle(file).setDescription("save")
                            .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    dm.enqueue(request);

                    Toast.makeText(context.getApplicationContext(), "Download Started", Toast.LENGTH_LONG).show();
                } catch (IllegalStateException ex) {
                    Toast.makeText(context.getApplicationContext(), "Storage Error", Toast.LENGTH_LONG).show();
                    ex.printStackTrace();
                } catch (Exception ex) {
                    Toast.makeText(context.getApplicationContext(), "Unable to save image", Toast.LENGTH_LONG).show();
                    ex.printStackTrace();
                }
            }
        }

        private void showDialog() {
            View v = ((Activity) context).getLayoutInflater().inflate(R.layout.more_bottomlayout, null);
            sheetBehavior.setContentView(v);
            close = sheetBehavior.findViewById(R.id.close);
            share_ln = sheetBehavior.findViewById(R.id.share_ln);
            delete_ln = sheetBehavior.findViewById(R.id.delete_ln);
            rename_ln = sheetBehavior.findViewById(R.id.rename_ln);
            download_ln = sheetBehavior.findViewById(R.id.download_ln);
            move_ln = sheetBehavior.findViewById(R.id.move_ln);
            rename_ll = sheetBehavior.findViewById(R.id.rename_ll);
            name_tv = sheetBehavior.findViewById(R.id.name_tv);
            rename_tv = sheetBehavior.findViewById(R.id.rename_tv);
            cancel_tv = sheetBehavior.findViewById(R.id.cancel_tv);
            option_ln = sheetBehavior.findViewById(R.id.option_ln);
            share_ln.setOnClickListener(this);
            if (types.contains(0)) {
                move_ln.setVisibility(View.GONE);
                download_ln.setVisibility(View.GONE);
            }
            close.setOnClickListener(this);
            cancel_tv.setOnClickListener(this);
            rename_ln.setOnClickListener(this);
            delete_ln.setOnClickListener(this);
            rename_tv.setOnClickListener(this);
            download_ln.setOnClickListener(this);
            move_ln.setOnClickListener(this);
            if (recent || shared) {
                move_ln.setVisibility(View.GONE);
            }
            name_tv.setText(itemList.get(getAdapterPosition()).name);
            if (sheetBehavior.isShowing())
                sheetBehavior.dismiss();
            sheetBehavior.show();
        }

        private void initCreate(int option, int create) {
            option_ln.setVisibility(option);
            rename_ll.setVisibility(create);
        }

        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.share_ln:
                    showShareDialog();
                    break;
                case R.id.move_ln:
                    if (ids == null) {
                        ids = new ArrayList<>();
                        ids.add(itemList.get(getAdapterPosition()).id);
                    }
                    View view = ((Activity) context).getLayoutInflater().inflate(R.layout.move_bottomlayout, null);
                    sheetBehavior.setContentView(view);
                    close = sheetBehavior.findViewById(R.id.close);
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            sheetBehavior.dismiss();
                        }
                    });
                    CustomRecyclerView files_rv = sheetBehavior.findViewById(R.id.files_rv);
                    files_rv.setLayoutManager(new LinearLayoutManager(context));
                    files_rv.setAdapter(new MoveAdapter(actionmode, context, ((Files) context).Folderlist, sheetBehavior, ids, folderdata));
                    if (sheetBehavior.isShowing())
                        sheetBehavior.dismiss();
                    sheetBehavior.show();
                    break;
                case R.id.close:
                    sheetBehavior.dismiss();
                    break;
                case R.id.cancel_tv:
                    initCreate(View.VISIBLE, View.GONE);
                    break;
                case R.id.rename_ln:
                    initCreate(View.GONE, View.VISIBLE);
                    break;
                case R.id.delete_ln:
                    if (ids == null) {
                        ids = new ArrayList<>();
                        ids.add(itemList.get(getAdapterPosition()).id);
                    }
                    if (shared)
                        deleteSharedFile();
                    else deleteFile();
                    break;
                case R.id.rename_tv:
                    if (!name_tv.getText().toString().isEmpty()) {
                        CommonUtills.progresshow(context);
                        ((Files) context).service.rename(name_tv.getText().toString(), itemList.get(getAdapterPosition()).type == 0 ? "folder" : "file", itemList.get(getAdapterPosition()).id).enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                                CommonUtills.progrescancel();
                                if (response.body() != null) {
                                    if (response.body().get("success").getAsBoolean()) {
                                        sheetBehavior.dismiss();
                                        if (all) {
                                            if (folderdata)
                                                ((Files) context).setFolderData();
                                            else
                                                ((Files) context).setData();
                                        } else if (recent) {
                                            ((Files) context).setrecentData();
                                        } else {
                                            ((Files) context).setSharedData();
                                        }
                                    }
                                    CommonUtills.alertDialog(context, response.body().get("message").getAsString());
                                } else
                                    CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                            }

                            @Override
                            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                                CommonUtills.progrescancel();
                                CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                            }
                        });
                    } else {
                        name_tv.setError("Name is required");
                        name_tv.requestFocus();
                    }
                    break;
                case R.id.send_tv:
                    if (email_et.getText().toString().trim().equals("")) {
                        email_et.setError(context.getString(R.string.emailerror));
                        email_et.requestFocus();
                    } else if (!Patterns.EMAIL_ADDRESS.matcher(email_et.getText().toString().trim()).matches()) {
                        email_et.setError(context.getString(R.string.emailaddresserror));
                        email_et.requestFocus();
                    } else {
                        if (ids == null) {
                            ids = new ArrayList<>();
                            ids.add(itemList.get(getAdapterPosition()).id);
                        }
                        CommonUtills.progresshow(context);
                        ((Files) context).service.shareemail(email_et.getText().toString(), itemList.get(getAdapterPosition()).type == 0 ? "folder" : "file", ids).enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                                CommonUtills.progrescancel();
                                sheetBehavior.dismiss();
                                if (response.body() != null) {
                                    if (response.body().get("success").getAsBoolean()) {
                                        if (actionmode != null)
                                            actionmode.finish();
                                        CommonUtills.alertDialog(context, "Data Shared Successfully");
                                    } else
                                        CommonUtills.alertDialog(context, response.body().get("message").getAsString());
                                } else
                                    CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                            }

                            @Override
                            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                                CommonUtills.progrescancel();
                                CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                            }
                        });
                    }
                    break;
                case R.id.download_ln:
                    sheetBehavior.dismiss();
                    if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ((Activity) context).requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    } else {
                        saveFile();
                    }
                    break;
                case R.id.link_ll:
                    sheetBehavior.dismiss();
                    if (urls == null) {
                        urls = new ArrayList<>();
                        urls.add(itemList.get(getAdapterPosition()).url);
                    }
                    int sdk = android.os.Build.VERSION.SDK_INT;
                    if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                        android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                        clipboard.setText(itemList.get(getAdapterPosition()).url);
                    } else {
                        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                        android.content.ClipData clip = android.content.ClipData.newPlainText("text label", getUrls());
                        clipboard.setPrimaryClip(clip);
                    }
                    Toast.makeText(context, "Link Copied to clipboard", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.other_ll:
                    sheetBehavior.dismiss();
                    if (urls == null) {
                        urls = new ArrayList<>();
                        urls.add(itemList.get(getAdapterPosition()).url);
                    }
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, getUrls());
                    sendIntent.setType("text/plain");
                    context.startActivity(sendIntent);
                    break;
                case R.id.mail_ll:
                    sheetBehavior.dismiss();
                    if (urls == null) {
                        urls = new ArrayList<>();
                        urls.add(itemList.get(getAdapterPosition()).url);
                    }
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setType("text/html").putExtra(Intent.EXTRA_SUBJECT, "Share File").putExtra(Intent.EXTRA_TEXT, getUrls());
                    final PackageManager pm = context.getPackageManager();
                    final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
                    String className = null;
                    for (final ResolveInfo info : matches) {
                        if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                            className = info.activityInfo.name;
                            if (className != null && !className.isEmpty()) {
                                break;
                            }
                        }
                    }
                    emailIntent.setClassName("com.google.android.gm", className);
                    context.startActivity(emailIntent);
                    break;
            }
        }

        private void deleteFile() {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Are you Sure?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    CommonUtills.progresshow(context);
                    ((Files) context).service.deletefile(itemList.get(getAdapterPosition()).type == 0 ? "folder" : "file", ids).enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                            CommonUtills.progrescancel();
                            if (response.body() != null) {
                                if (response.body().get("success").getAsBoolean()) {
                                    if (actionmode != null)
                                        actionmode.finish();
                                    else {
                                        if (all) {
                                            if (folderdata)
                                                ((Files) context).setFolderData();
                                            else
                                                ((Files) context).setData();
                                        } else if (recent) {
                                            ((Files) context).setrecentData();
                                        } else {
                                            ((Files) context).setSharedData();
                                        }
                                    }
                                }
                                CommonUtills.alertDialog(context, response.body().get("message").getAsString());
                            } else
                                CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                        }

                        @Override
                        public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                            CommonUtills.progrescancel();
                            CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                        }
                    });
                    sheetBehavior.dismiss();
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

        private void deleteSharedFile() {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Are you Sure?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    CommonUtills.progresshow(context);
                    ((Files) context).service.deletesharedfile(itemList.get(getAdapterPosition()).type == 0 ? "folder" : "file", ids).enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                            CommonUtills.progrescancel();
                            if (response.body() != null) {
                                if (response.body().get("success").getAsBoolean()) {
                                    if (actionmode != null)
                                        actionmode.finish();
                                    else {
                                        if (all) {
                                            if (folderdata)
                                                ((Files) context).setFolderData();
                                            else
                                                ((Files) context).setData();
                                        } else if (recent) {
                                            ((Files) context).setrecentData();
                                        } else {
                                            ((Files) context).setSharedData();
                                        }
                                    }
                                }
                                CommonUtills.alertDialog(context, response.body().get("message").getAsString());
                            } else
                                CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                        }

                        @Override
                        public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                            CommonUtills.progrescancel();
                            CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                        }
                    });
                    sheetBehavior.dismiss();
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

        private String getUrls() {
            String finalurl = null;
            for (String url : urls) {
                if (finalurl == null)
                    finalurl = url;
                else finalurl = finalurl + "\n" + url;
            }
            return finalurl;
        }

        private void showShareDialog() {
            View v = ((Activity) context).getLayoutInflater().inflate(R.layout.share_bottomlayout, null);
            sheetBehavior.setContentView(v);
            close = sheetBehavior.findViewById(R.id.close);
            name_tv = sheetBehavior.findViewById(R.id.name_tv);
            send_tv = sheetBehavior.findViewById(R.id.send_tv);
            link_ll = sheetBehavior.findViewById(R.id.link_ll);
            LinearLayout other_ln = sheetBehavior.findViewById(R.id.other_ln);
            other_ll = sheetBehavior.findViewById(R.id.other_ll);
            mail_ll = sheetBehavior.findViewById(R.id.mail_ll);
            email_et = sheetBehavior.findViewById(R.id.email_et);
            message_et = sheetBehavior.findViewById(R.id.message_et);
            link_ll.setOnClickListener(this);
            other_ll.setOnClickListener(this);
            close.setOnClickListener(this);
            send_tv.setOnClickListener(this);
            mail_ll.setOnClickListener(this);
            if ((types.size() > 0 && types.contains(0)) || itemList.get(getAdapterPosition()).type == 0)
                other_ln.setVisibility(View.GONE);
            if (sheetBehavior.isShowing())
                sheetBehavior.dismiss();
            sheetBehavior.show();
        }
    }
}