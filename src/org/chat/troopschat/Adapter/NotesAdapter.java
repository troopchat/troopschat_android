package org.chat.troopschat.Adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.chat.troopschat.Activities.NoteDetail;
import org.chat.troopschat.Activities.Notes;
import org.chat.troopschat.Pojo.NotesPojo;
import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.MyViewHolder> {
    boolean trash;
    ArrayList<String> ids = new ArrayList<>();
    ActionMode actionmode;
    boolean isMultiSelect;
    LinearLayout toolbar;
    List<String> colors = new ArrayList<>();
    Menu menu;
    private Context context;
    private List<NotesPojo.Datum> itemList;

    public NotesAdapter(Context context, LinearLayout toolbar, List<NotesPojo.Datum> itemList, boolean trash) {
        this.context = context;
        this.itemList = itemList;
        this.trash = trash;
        this.toolbar = toolbar;
        colors.add("#ff6900");
        colors.add("#23c0e9");
        colors.add("#ffca00");
        colors.add("#E91E63");
        colors.add("#FDF4B3");
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.noteslist_rv, viewGroup, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        if (trash && !isMultiSelect) {
            myViewHolder.delete.setVisibility(View.VISIBLE);
            myViewHolder.restore.setVisibility(View.VISIBLE);
        } else {
            myViewHolder.delete.setVisibility(View.GONE);
            myViewHolder.restore.setVisibility(View.GONE);
        }
        myViewHolder.date_tv.setText(itemList.get(i).createdAt);
        myViewHolder.name_tv.setText(itemList.get(i).title);
        myViewHolder.check.setVisibility(itemList.get(i).status==1 ? View.VISIBLE : View.GONE);
        myViewHolder.lock.setVisibility(itemList.get(i).noteLock==1 ? View.VISIBLE : View.GONE);
        myViewHolder.main_cv.setCardBackgroundColor(Color.parseColor(itemList.get(i).color!=null?itemList.get(i).color:"#FDF4B3"));
        if (itemList.get(i).status==1)
            myViewHolder.name_tv.setPaintFlags(myViewHolder.name_tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    private void delete() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you Sure?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((Notes) context).service.trashnote(ids).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                        CommonUtills.progrescancel();
                        if (response.body() != null) {
                            actionmode.finish();
                            CommonUtills.alertDialog(context, "Notes moved to trash");
                        } else
                            CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                    }

                    @Override
                    public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                        CommonUtills.progrescancel();
                        CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                    }
                });
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void deletetrash() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you Sure?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((Notes) context).service.deletenote(ids).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                        CommonUtills.progrescancel();
                        if (response.body() != null) {
                            if (response.body().get("success").getAsBoolean()) {
                                if (actionmode != null)
                                    actionmode.finish();
                                else {
                                    if (!trash)
                                        ((Notes) context).setData();
                                    else
                                        ((Notes) context).setTrashData();
                                }
                            }
                        } else
                            CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                    }

                    @Override
                    public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                        CommonUtills.progrescancel();
                        CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                    }
                });
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showDialog() {
        final Dialog dialog;
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        LayoutInflater inflater = ((AppCompatActivity) context).getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.color_popup, null);
        alert.setView(alertLayout);
        alert.setCancelable(false);
        dialog = alert.create();
        GridView gridView = alertLayout.findViewById(R.id.attributes_list);
        gridView.setVisibility(View.VISIBLE);
        gridView.setAdapter(new ColorsAdapter(context, "note", colors, ids, actionmode, dialog));
        alertLayout.findViewById(R.id.cancel_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name_tv, date_tv;
        ImageView check, delete, restore, lock;
        CardView main_cv;

        MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            name_tv = itemView.findViewById(R.id.name_tv);
            lock = itemView.findViewById(R.id.lock);
            restore = itemView.findViewById(R.id.restore);
            date_tv = itemView.findViewById(R.id.date_tv);
            main_cv = itemView.findViewById(R.id.main_cv);
            check = itemView.findViewById(R.id.check);
            delete = itemView.findViewById(R.id.delete);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isMultiSelect) {
                        if (ids.contains(itemList.get(getAdapterPosition()).noteId)) {
                            ids.remove(itemList.get(getAdapterPosition()).noteId);
                            itemView.setBackgroundColor(Color.WHITE);
                        } else {
                            ids.add(itemList.get(getAdapterPosition()).noteId);
                            if (itemList.get(getAdapterPosition()).noteLock==1)
                            {
                                menu.getItem(0).setVisible(false);
                            }
                            itemView.setBackgroundColor(Color.parseColor("#A9A9A9"));
                        }
                    } else if (!trash) {
                        Intent intent = new Intent(context, NoteDetail.class);
                        intent.putExtra("title", itemList.get(getAdapterPosition()).title);
                        intent.putExtra("note", itemList.get(getAdapterPosition()).discription);
                        intent.putExtra("color", itemList.get(getAdapterPosition()).color!=null?itemList.get(getAdapterPosition()).color:"#FDF4B3");
                        intent.putExtra("date", itemList.get(getAdapterPosition()).createdAt);
                        intent.putExtra("lock", itemList.get(getAdapterPosition()).noteLock==1);
                        intent.putExtra("status", itemList.get(getAdapterPosition()).status==1);
                        intent.putExtra("id", itemList.get(getAdapterPosition()).noteId);
                        context.startActivity(intent);
                    }
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    isMultiSelect = true;
                    ids.add(itemList.get(getAdapterPosition()).noteId);
                    itemView.setBackgroundColor(Color.parseColor("#A9A9A9"));
                    actionmode = ((AppCompatActivity) context).startActionMode(new android.view.ActionMode.Callback() {
                        @Override
                        public boolean onCreateActionMode(android.view.ActionMode mode, Menu menu) {
                            NotesAdapter.this.menu = menu;
                            MenuInflater menuInflater = mode.getMenuInflater();
                            menuInflater.inflate(R.menu.actionmenu, menu);
                            menu.getItem(2).setVisible(false);
                            if (itemList.get(getAdapterPosition()).noteLock==1)
                            {
                                menu.getItem(0).setVisible(false);
                            }
                            toolbar.setVisibility(View.GONE);
                            if (trash)
                                menu.getItem(1).setVisible(false);
                            else
                                menu.getItem(3).setVisible(false);
                            if (trash)
                                notifyDataSetChanged();
                            return true;
                        }

                        @Override
                        public boolean onPrepareActionMode(android.view.ActionMode mode, Menu menu) {
                            return false;
                        }

                        @Override
                        public boolean onActionItemClicked(android.view.ActionMode mode, MenuItem item) {
                            int id = item.getItemId();
                            if (id == R.id.delete) {
                                if (!trash)
                                    delete();
                                else
                                    deletetrash();
                                return true;
                            } else if (id == R.id.color) {
                                showDialog();
                            } else if (id == R.id.restore) {
                                restoreItems();
                            }
                            return true;
                        }

                        @Override
                        public void onDestroyActionMode(android.view.ActionMode mode) {
                            toolbar.setVisibility(View.VISIBLE);
                            if (!trash)
                                ((Notes) context).setData();
                            else
                                ((Notes) context).setTrashData();
                        }
                    });
                    return true;
                }
            });
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ids.add(itemList.get(getAdapterPosition()).noteId);
                    deletetrash();
                }
            });
            restore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ids.add(itemList.get(getAdapterPosition()).noteId);
                    restoreItems();
                }
            });
        }

        private void restoreItems() {
            ((Notes) context).service.restoretrash(ids).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                    if (response.body() != null) {
                        if (actionmode != null)
                            actionmode.finish();
                        else
                            ((Notes) context).setTrashData();
                    } else
                        CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                }

                @Override
                public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                    CommonUtills.progrescancel();
                    CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                }
            });
        }
    }
}