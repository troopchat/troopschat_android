package org.chat.troopschat.Adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.chat.troopschat.Activities.CheckListDetail;
import org.chat.troopschat.Activities.Notes;
import org.chat.troopschat.Pojo.CheckListPojo;
import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChecklistAdapter extends RecyclerView.Adapter<ChecklistAdapter.MyViewHolder> {
    LinearLayout toolbar;
    List<String> colors = new ArrayList<>();
    private boolean trash;
    private ArrayList<String> ids = new ArrayList<>();
    private ActionMode actionmode;
    private boolean isMultiSelect;
    private Context context;
    private List<CheckListPojo.Datum> itemList;
    Menu menu;

    public ChecklistAdapter(Context context, LinearLayout toolbar, List<CheckListPojo.Datum> itemList, boolean trash) {
        this.context = context;
        this.itemList = itemList;
        this.trash = trash;
        this.toolbar = toolbar;
        colors.add("#ff6900");
        colors.add("#23c0e9");
        colors.add("#ffca00");
        colors.add("#E91E63");
        colors.add("#FDF4B3");
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.noteslist_rv, viewGroup, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        if (trash && !isMultiSelect) {
            myViewHolder.delete.setVisibility(View.VISIBLE);
            myViewHolder.restore.setVisibility(View.VISIBLE);
        } else {
            myViewHolder.delete.setVisibility(View.GONE);
            myViewHolder.restore.setVisibility(View.GONE);
        }
        myViewHolder.date_tv.setText(itemList.get(i).createdAt);
        myViewHolder.lock.setVisibility(itemList.get(i).checkListLock==1 ? View.VISIBLE : View.GONE);
        myViewHolder.name_tv.setText(itemList.get(i).checkListTitle);
        try {
            myViewHolder.main_cv.setCardBackgroundColor(Color.parseColor(itemList.get(i).checkListColor));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    private void delete() {
        CommonUtills.progresshow(context);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you Sure?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((Notes) context).service.deletechecklist(ids).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                        CommonUtills.progrescancel();
                        if (response.body() != null) {
                            if (response.body().get("success").getAsBoolean()) {
                                if (actionmode != null)
                                    actionmode.finish();
                                else
                                    ((Notes) context).setcheckTrashData();
                            }
                            CommonUtills.alertDialog(context, response.body().get("message").getAsString());
                        } else
                            CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                    }

                    @Override
                    public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                        CommonUtills.progrescancel();
                        CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                    }
                });
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void trash() {
        CommonUtills.progresshow(context);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you Sure?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((Notes) context).service.trashchecklist(ids).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                        CommonUtills.progrescancel();
                        if (response.body() != null) {
                            if (response.body().get("success").getAsBoolean()) {
                                actionmode.finish();
                            }
                            CommonUtills.alertDialog(context, response.body().get("message").getAsString());
                        } else
                            CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                    }

                    @Override
                    public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                        CommonUtills.progrescancel();
                        CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                    }
                });
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void showDialog() {
        final Dialog dialog;
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        LayoutInflater inflater = ((AppCompatActivity) context).getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.color_popup, null);
        alert.setView(alertLayout);
        alert.setCancelable(false);
        dialog = alert.create();
        GridView gridView = alertLayout.findViewById(R.id.attributes_list);
        gridView.setVisibility(View.VISIBLE);
        gridView.setAdapter(new ColorsAdapter(context, "checklist", colors, ids, actionmode, dialog));
        alertLayout.findViewById(R.id.cancel_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name_tv, date_tv;
        ImageView check, lock, delete, restore;
        CardView main_cv;

        MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            name_tv = itemView.findViewById(R.id.name_tv);
            date_tv = itemView.findViewById(R.id.date_tv);
            lock = itemView.findViewById(R.id.lock);
            main_cv = itemView.findViewById(R.id.main_cv);
            check = itemView.findViewById(R.id.check);
            delete = itemView.findViewById(R.id.delete);
            restore = itemView.findViewById(R.id.restore);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ids.add(itemList.get(getAdapterPosition()).checkListId);
                    delete();
                }
            });
            restore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ids.add(itemList.get(getAdapterPosition()).checkListId);
                    restoreItems();
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    isMultiSelect = true;
                    ids.add(itemList.get(getAdapterPosition()).checkListId);
                    itemView.setBackgroundColor(Color.parseColor("#A9A9A9"));
                    actionmode = ((AppCompatActivity) context).startActionMode(new android.view.ActionMode.Callback() {
                        @Override
                        public boolean onCreateActionMode(android.view.ActionMode mode, Menu menu) {
                            ChecklistAdapter.this.menu = menu;
                            MenuInflater menuInflater = mode.getMenuInflater();
                            menuInflater.inflate(R.menu.actionmenu, menu);
                            menu.getItem(2).setVisible(false);
                            toolbar.setVisibility(View.GONE);
                            if (itemList.get(getAdapterPosition()).checkListLock==1)
                            {
                                menu.getItem(0).setVisible(false);
                            }
                            if (trash)
                                menu.getItem(1).setVisible(false);
                            else
                                menu.getItem(3).setVisible(false);
                            if (trash)
                                notifyDataSetChanged();
                            return true;
                        }

                        @Override
                        public boolean onPrepareActionMode(android.view.ActionMode mode, Menu menu) {
                            return false;
                        }

                        @Override
                        public boolean onActionItemClicked(android.view.ActionMode mode, MenuItem item) {
                            int id = item.getItemId();
                            if (id == R.id.delete) {
                                if (trash)
                                    delete();
                                else
                                    trash();
                                return true;
                            } else if (id == R.id.color) {
                                showDialog();
                            } else if (id == R.id.restore) {
                                restoreItems();
                            }
                            return true;
                        }

                        @Override
                        public void onDestroyActionMode(android.view.ActionMode mode) {
                            toolbar.setVisibility(View.VISIBLE);
                            if (!trash)
                                ((Notes) context).setChecklistData();
                            else
                                ((Notes) context).setcheckTrashData();
                        }
                    });
                    return true;
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isMultiSelect) {
                        if (ids.contains(itemList.get(getAdapterPosition()).checkListId)) {
                            ids.remove(itemList.get(getAdapterPosition()).checkListId);
                            itemView.setBackgroundColor(Color.WHITE);
                        } else {
                            if (itemList.get(getAdapterPosition()).checkListLock==1)
                            {
                                menu.getItem(0).setVisible(false);
                            }
                            ids.add(itemList.get(getAdapterPosition()).checkListId);
                            itemView.setBackgroundColor(Color.parseColor("#A9A9A9"));
                        }
                    } else if (!trash) {
                        Intent intent = new Intent(context, CheckListDetail.class);
                        intent.putExtra("title", itemList.get(getAdapterPosition()).checkListTitle);
                        intent.putExtra("color", itemList.get(getAdapterPosition()).checkListColor);
                        intent.putExtra("date", itemList.get(getAdapterPosition()).createdAt);
                        intent.putExtra("lock", itemList.get(getAdapterPosition()).checkListLock==1);
                        intent.putExtra("id", itemList.get(getAdapterPosition()).checkListId);
                        context.startActivity(intent);
                    }
                }
            });
        }

        private void restoreItems() {
            CommonUtills.progresshow(context);
            ((Notes) context).service.restorechecktrash(ids).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                    CommonUtills.progrescancel();
                    if (response.body() != null) {
                        if (actionmode != null)
                            actionmode.finish();
                        else
                            ((Notes) context).setcheckTrashData();
                    } else
                        CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                }

                @Override
                public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                    CommonUtills.progrescancel();
                    CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                }
            });
        }
    }
}