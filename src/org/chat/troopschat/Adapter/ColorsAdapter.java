package org.chat.troopschat.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.chat.troopschat.Activities.Notes;
import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ColorsAdapter extends BaseAdapter {
    Context context;
    List<String> colorlist;
    List<String> ids;
    Dialog dialog;
    LinearLayout mainln;
    ActionMode actionMode;
    String type;

    public ColorsAdapter(Context context, List<String> colorlist, Dialog dialog, LinearLayout mainln) {
        this.context = context;
        this.colorlist = colorlist;
        this.dialog = dialog;
        this.mainln = mainln;
    }

    public ColorsAdapter(Context context, String type, List<String> colorlist, List<String> ids, ActionMode actionMode, Dialog dialog) {
        this.context = context;
        this.colorlist = colorlist;
        this.ids = ids;
        this.actionMode = actionMode;
        this.dialog = dialog;
        this.type = type;
    }

    @Override
    public int getCount() {
        return colorlist.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.productscolor_list, viewGroup, false);
        TextView colorpreview_tv = convertView.findViewById(R.id.colorpreview_tv);
        GradientDrawable shape = (GradientDrawable) colorpreview_tv.getBackground();
        shape.setColor(Color.parseColor(colorlist.get(i)));
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mainln != null)
                    mainln.setBackgroundColor(Color.parseColor(colorlist.get(i)));
                else {
                    Call<JsonObject> callback;
                    if (type.equalsIgnoreCase("note"))
                    callback=((Notes) context).service.editColor(ids, colorlist.get(i));
                    else callback=((Notes) context).service.editcheckColor(ids, colorlist.get(i));
                    callback.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                            CommonUtills.progrescancel();
                            if (response.body() != null) {
                                if (response.body().get("success").getAsBoolean()) {
                                    if (actionMode != null)
                                        actionMode.finish();
                                    else {
                                        ((Notes) context).setData();
                                    }
                                }
                            } else
                                CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                        }

                        @Override
                        public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                            CommonUtills.progrescancel();
                            CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                        }
                    });
                }
                dialog.dismiss();
            }
        });
        return convertView;
    }
}