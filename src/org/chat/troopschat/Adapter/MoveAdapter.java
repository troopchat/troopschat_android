package org.chat.troopschat.Adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.chat.troopschat.Activities.Files;
import org.chat.troopschat.Pojo.FilesPojo;
import org.chat.troopschat.R;
import org.chat.troopschat.Utils.CommonUtills;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MoveAdapter extends RecyclerView.Adapter<MoveAdapter.MyViewHolder> {
    BottomSheetDialog sheetBehavior;
    ArrayList<String> ids;
    ActionMode actionmode;
    private Context context;
    private List<FilesPojo> itemList;
    private boolean folderdata;

    public MoveAdapter(ActionMode actionmode, Context context, List<FilesPojo> itemList, BottomSheetDialog sheetBehavior, ArrayList<String> ids, boolean folderdata) {
        this.context = context;
        this.itemList = itemList;
        this.sheetBehavior = sheetBehavior;
        this.folderdata = folderdata;
        this.ids = ids;
        this.actionmode = actionmode;
        setHasStableIds(true);
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.movelist_rv, viewGroup, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        if (i != 0)
            myViewHolder.date_tv.setText(itemList.get(i).date);
        else myViewHolder.date_tv.setVisibility(View.GONE);
        myViewHolder.name_tv.setText(itemList.get(i).name);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView file_iv;
        TextView name_tv, date_tv;

        MyViewHolder(View itemView) {
            super(itemView);
            file_iv = itemView.findViewById(R.id.file_iv);
            name_tv = itemView.findViewById(R.id.name_tv);
            date_tv = itemView.findViewById(R.id.date_tv);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CommonUtills.progresshow(context);
                    ((Files) context).service.moveFile(ids, itemList.get(getAdapterPosition()).id).enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                            CommonUtills.progrescancel();
                            if (response.body() != null) {
                                if (response.body().get("success").getAsBoolean()) {
                                    if (actionmode != null)
                                        actionmode.finish();
                                    else {
                                        if (folderdata)
                                            ((Files) context).setFolderData();
                                        else
                                            ((Files) context).setData();
                                    }
                                }
                                CommonUtills.alertDialog(context, "Data moved successfully");
                            } else
                                CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                        }

                        @Override
                        public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                            CommonUtills.progrescancel();
                            CommonUtills.alertDialog(context, "Something went Wrong Please try again");
                        }
                    });
                    sheetBehavior.dismiss();
                }
            });
        }
    }
}