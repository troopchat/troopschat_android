package org.chat.troopschat.qr;

public interface ScanListener {
  public void onQrDataFound(String data);
}
