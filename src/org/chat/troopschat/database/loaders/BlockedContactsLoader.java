package org.chat.troopschat.database.loaders;

import android.content.Context;
import android.database.Cursor;

import org.chat.troopschat.database.DatabaseFactory;
import org.chat.troopschat.util.AbstractCursorLoader;

public class BlockedContactsLoader extends AbstractCursorLoader {

  public BlockedContactsLoader(Context context) {
    super(context);
  }

  @Override
  public Cursor getCursor() {
    return DatabaseFactory.getRecipientDatabase(getContext())
                          .getBlocked();
  }

}
