package org.chat.troopschat.giph.ui;


import android.os.Bundle;
import android.support.v4.content.Loader;

import org.chat.troopschat.giph.model.GiphyImage;
import org.chat.troopschat.giph.net.GiphyGifLoader;

import java.util.List;

public class GiphyGifFragment extends GiphyFragment {

  @Override
  public Loader<List<GiphyImage>> onCreateLoader(int id, Bundle args) {
    return new GiphyGifLoader(getActivity(), searchString);
  }

}
