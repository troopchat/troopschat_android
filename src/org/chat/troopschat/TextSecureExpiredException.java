package org.chat.troopschat;

public class TextSecureExpiredException extends Exception {
  public TextSecureExpiredException(String message) {
    super(message);
  }
}
